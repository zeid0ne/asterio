'use strict';

	// Player inputs manager (cursor and keys)
module.exports = global.Input = class Input {

	constructor () {
			// Keyboard code
		this.keyCode = {
			up					: 90,	// Z
			down				: 83,	// S 
			left				: 81,	// Q
			right				: 68,	// D
			r_left				: 37,	// arrow left
			r_right				: 39, 	// arrow right
//			shoot				: 38,	// arrow up
//			switch_auto_shoot	: 32	// space
		};

			// Map for key state (pressed or not)
		this.keyMap = new Map();
		for (let name in this.keyCode) {
			const code = this.keyCode[name];
			this.keyMap.set(code, new Key(name, code));
		}

			// Cursor
		this.cursor = new Cursor();
	}

		// TRUE the keycode exist in keyboard configuration
	hasKeyCode (keyCode) {
		return this.keyMap.has(keyCode);
	}
		// Set a key state 
	setKeyActivation (keyCode, boolean) {
		this.keyMap.get(keyCode).setActivation(boolean);
		this.cursorActivationUpdate();
		// TODO see below
//		this.cursorDownUpdate();
	}
		// If player is rotating, desactivate his orientation by cursor
	cursorActivationUpdate () {
		if (this.keyMap.get(this.keyCode.r_left) || 
			this.keyMap.get(this.keyCode.r_right)) {
			this.cursor.setActivation(false);
		}
	}
		// Actualise l'état du clic du curseur
	// cursorDownUpdate () {
	// 	var boolean = this.keyMap.get(this.keyCode.shoot);
	// 	if (boolean) {
	// 		this.cursor.setDown(true);
	// 	} else if (this.cursor.isDown) {
	// 		this.cursor.setDown(false);
	// 	}
	// }

		// Actualise le temps d'une action
//	refreshKeyTime () {

			// On parcoure le tableau du temps des actions
//		for (var i in this.keyTime) {

				// On incrémente le temps si l'action est en cours
				// _TODO : this.keyTime[i] < Constant.MAX_TRUC
//			if (this.keyTime[i] > 0 && this.keyTime[i] < 10) {
//				if (i == 'shoot' || i == 'rotate_left' || i == 'rotate_right') {
//					this.keyTime[i] == 1;
//				}
//				else {
//					this.keyTime[i]++;
//				}
//			}
//		}
//	}

		// Actualise l'état du tir auto
		// _TODO : refaire pcq c'est bizarre de gérer comme ça...
//	refreshAutoShoot () {
//		if (this.keyTime.switch_auto_shoot == 2) {
//			this.auto_shoot = (this.auto_shoot+1)%2;
//		}
//	}

/*		// Actualise le tir
	refreshShoot () {
		// Si on vient de cliquer sur une touche de tir
		if (this.clic_shoot) {
			this.clic_shoot = false;	// On désactive le clic ponctuel
			if (!this.clic_next) {		// Si l'on avait pas encore enregistré de tir
				this.clic_next = true;	// On l'enregistre
			}							// Sinon on amène directement le délai de tir au maximum
			else {						// Ce qui aura pour effet de le réinitialiser et de le
				this.time_shoot = this.max_time_shoot+1;	// relancer pour réduire le délai
			}							// de la cadence automatique
		}
		// Si un tir est en cours, le délai s'actualise
		if (this.keyTime.shoot || this.time_shoot > 0 
			|| this.clic_next || this.auto_shoot) {
			this.time_shoot++;
		}
		// Lorsque le délai arrive au minimum et qu'un tir a été enregistré
		// (en gros que le joueur a cliqué vite plusieurs fois d'affilé)*
		// Ou alors que le délai arrive au maximum
		if ((this.time_shoot > this.min_time_shoot && this.clic_next)
			|| (this.time_shoot > this.max_time_shoot)) {
			// On réinitialise le délai et on désactive le tir en cours
			this.time_shoot = 0;
			this.clic_next = false;
		}
	}
*/

		// Actualise l'état des actions
//	refreshKeyState (key, state) {

			// On parcoure le tableau des codes de touches
			// _TODO optimiser avec un accès direct (Map.get(keyCode) = (keyName, keyTime))
//		for (var i in this.keyCode) {

				// Lorsqu'on a trouvé la touche correspondante
//			if (key === this.keyCode[i]) {

				// On défini son état
				// VRAI si le temps est à zéro et si on presse la touche
				// FAUX si son temps est supérieur à zéro et si on relache la touche
//				if ((this.keyTime[i] === 0 && state === true) 
//					|| (this.keyTime[i] > 0 && state === false)) {
//					this.setKeyState(i, state);
//				} break;
//			}
//		}

			// Cas spécifique pour le tir
			// _TODO : opti ou changer le système foireux
//		if (key == this.keyCode.shoot) {
//			this.cursor_activate = false;
//			this.clic_shoot = true;
//		}
//	}
}