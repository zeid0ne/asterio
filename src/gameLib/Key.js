'use strict';

module.exports = global.Key = class Key {

	constructor (name, code) {
		this.name 		= name;
		this.code 		= code;
		this.isActive 	= false;
	}

	//--- Activatable
	setActivation (boolean) {
		this.isActive = boolean;
	}
}