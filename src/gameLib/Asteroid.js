'use strict';

require('./Entity.js')

	// Asteroid
module.exports = global.Asteroid = class Asteroid extends Entity {
	
		// - hitbox : Polygon or Circle
		// - velocity : Vector
		// - rotationSpeed : in angle/second
		// - size : Asteroid.SIZE_MIN/MAX
		// - gunConfig : for the bonus
	constructor(hitbox, velocity, rotationSpeed, size, first, gunConfig) {
			// bound = rectangular bouding box
		const bound = Asteroid.RADIUS_MIN + Asteroid.RADIUS_COEF*size;
		super(bound, hitbox, velocity, rotationSpeed);
		
		this.size 		= size;
		this.gunConfig 	= gunConfig;

			// TRUE when the asteroid is splitting by bullet collision
		this.isSplitting = false;

			// True if the asteroid is generated from hole (!= by split)
		this.first = first;
		if (View.DRAW_ANIMATION) {
			this.time = 0;
		}
	}
		// Return irregular polygon
	static newHitbox (origin, size) {
		const nbSide = Utility.random(Asteroid.NB_SIDE_MIN, Asteroid.NB_SIDE_MAX);
		let nbGeneratedSide = 0;
		const degMin = 360/nbSide;
		const radius = Asteroid.RADIUS_MIN + Asteroid.RADIUS_COEF*size;
		const xList = [];
		const yList = [];

		for (let deg = 0; deg < 360 && nbGeneratedSide < nbSide; deg++) {
			let ran = Utility.random(
				10 + Asteroid.NB_SIDE_MAX*10 - nbSide*10);
			if (deg >= degMin*nbGeneratedSide || 
				(!ran && deg >= degMin*(nbGeneratedSide - 1) + 30)) {
				let vector = new Vector(radius, Utility.toRad(deg));
				xList.push(vector.x);
				yList.push(vector.y);
				deg = degMin*nbGeneratedSide;
				nbGeneratedSide++;
			}
		}

		return new Polygon(xList, yList, origin);
	}
		// Return a new Asteroid from hole
	static newAsteroid (hole, isBonus) {
		let size;
		let gunConfig;
			// If is bonus
		if (isBonus) {
			size = 1;
			gunConfig = Utility.randomInMap(GunConfig.MAP);
		} else {
			size = Utility.random(1, Asteroid.SIZE_MAX);
		}

		const speed = (Asteroid.SIZE_MAX + 1 - size)*Asteroid.SPEED_COEF;
		let origin;
		let angle;

			// From the Space border
		if (hole instanceof Space) {
			origin = hole.newBorderPoint();
			const target = hole.newBorderPoint();
			angle = origin.angle(target);
		}
			// Else from a Blackhole
		else {
			origin = hole.hitbox.cloneOrigin();
			angle = Utility.toRad(Utility.random(359));
		}

		const hitbox = Asteroid.newHitbox(origin, size);
		const velocity = new Vector(speed, angle);
			// Between 15 and 20 rotation/second
		const rotationSpeed = Math.PI2/Utility.random(15, 20) *
			(Utility.random(1)*2 - 1);
		
		return new Asteroid(
			hitbox, velocity, rotationSpeed, size, true, gunConfig);
	}
		// Return the score value
	getScore () {
		return Asteroid.SIZE_MAX + 1 - this.size;
	}
		// Return 2 smaller Asteroids in direction of the bullet who shoot him
	split (bullet) {
		this.isSplitting = true;
			// If this is already the smallest, return nothing
		if (this.size === 1) {
			return [];
		}

		const asteroidList = [];
		const size = this.size - 1;
		const speed	= (Asteroid.SIZE_MAX + 1 - size)*Asteroid.SPEED_COEF;
		for (let i = -1; i < 2; i += 2) {
			const angle = 
				i*(Math.PI/16 + Math.PI/128*Utility.random(8));
			const direction = bullet.velocity.direction + angle;
			const velocity 	= new Vector(speed, direction);

			const origin = this.hitbox.cloneOrigin();
			const hitbox = Asteroid.newHitbox(origin, size);

			const asteroid = new Asteroid(
				hitbox, velocity, this.rotationSpeed, size);
			asteroidList.push(asteroid);
		}
		return asteroidList;
	}
		// TRUE if is Bonus
	isBonus () {
		return (this.gunConfig !== undefined);
	}
		// Return the color id of the asteroid according to his bonus
	getColorId () {
		if (this.isBonus()) {
			return this.gunConfig.colorId;
		} else {
			return Utility.GRAY;
		}
	}

	//--- Drawable
	// @Override
	draw (view, dt) {
		this.drawPhysic(view);

		if (View.DRAW_VIEW) {
				// Polygon faces
			for (let i = 0, l = this.ghost.hitbox.pointList.length; 
				i < l; i++) {
					// Triangle
				const i2 = (i+1)%l;
				const triangle = new Polygon(
					[this.ghost.hitbox.x, 
						this.ghost.hitbox.pointList[i].x,
						this.ghost.hitbox.pointList[i2].x],
					[this.ghost.hitbox.y,
						this.ghost.hitbox.pointList[i].y,
						this.ghost.hitbox.pointList[i2].y]);
					// Color, shade according to triangle orientation
				let shade = 10 + Math.floor(Utility.toDeg(
					Math.abs(this.ghost.hitbox.angle(
						this.ghost.hitbox.pointList[i]
					)) +
					Math.abs(this.ghost.hitbox.angle(
						this.ghost.hitbox.pointList[i2]
					))
				)/3);
					// Fade animation : modify shade
				if (View.DRAW_ANIMATION && this.first && this.time < 50*this.size) {
					const opacity = this.time / (50*this.size);
					shade = Math.round(shade*opacity);
					this.time += dt;
				}
				const color = Utility.getRgbaColor(this.getColorId(), shade);
				triangle.draw(view, color);
			}
				// Black outline
			this.ghost.hitbox.draw(view, 'black', View.STROKE);
		}

		this.ghost.velocity.draw(view, this.ghost.hitbox);
	}
}