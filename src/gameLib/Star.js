'use strict';

module.exports = global.Star = class Star extends Point {

	constructor (x, y, depth) {
		super(x, y);

			// To calculate parallax
		this.depthCoef = depth/Space.DEPTH;

			// deeper => smaller 
		this.radius = Star.RADIUS_MAX - Star.RADIUS_DELTA
			* ((depth - Star.DEPTH_MIN) / Star.DEPTH_DELTA);

			// Point to display (deformed by holes)
		this.ghostPoint = new Point();
	}

	//--- Drawable
	// @Override
	draw (view, space, blackHoleMap) {
			// Reset ghost point according to watched point in view
		if (view.watched.ghost) {
			this.ghostPoint.setPosition(
				this.x + this.depthCoef*view.watched.ghost.hitbox.x,
				this.y + this.depthCoef*view.watched.ghost.hitbox.y);
		} else if (view.watched instanceof Entity) {
			this.ghostPoint.setPosition(
				this.x + this.depthCoef*view.watched.hitbox.x,
				this.y + this.depthCoef*view.watched.hitbox.y);
		} else if (view.watched instanceof Point) {
			this.ghostPoint.setPosition(
				this.x + this.depthCoef*view.watched.x, 
				this.y + this.depthCoef*view.watched.y);
		} else {
			this.ghostPoint.setPosition(0, 0);
		}
		
			// Blink animation, enlarge & shade
		let radius = this.radius;
		let color = 'white';
		if (View.DRAW_ANIMATION) {
			if(!Utility.random(2500)) {
				radius += 3;
			}
			if (!Utility.random(2)) {
				color = 'rgba(255, 255, 255, .8)';
			}
		}

			// If outside of space
		if (!space.isInCoreRadius(this.ghostPoint)) {
			space.drawStarInGravityRadius(view, this, color, radius);
			return;
		}
			// Else if in a black hole
		for (let [id, blackHole] of blackHoleMap) {
			if (blackHole.contains(this.ghostPoint)) {
				blackHole.drawStarInGravityRadius(view, this, color, radius);		
				return;
			}
		}
			// Else classic point draw
		this.ghostPoint.draw(view, color, radius);
	}
}