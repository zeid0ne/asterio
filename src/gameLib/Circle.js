'use strict';

require('./Point.js');

module.exports = global.Circle = class Circle extends Point {

	constructor (x, y, radius) {
		super(x, y);
		this.radius = radius;
	}

	//--- Movable (same as super classe)

	//--- Hitbox
		// TRUE if the point is in hitbox
		// - point : Point
	contains (point) {
		return (this.near(point, this.radius));
	}
		// TRUE if hitboxes touch each other
		// - hitbox : Hitbox
	collision (hitbox) {
		if (hitbox instanceof Polygon) {
			return hitbox.collision(this);
		} else if (hitbox instanceof Circle) {
			return (this.near(hitbox, this.radius + hitbox.radius));
		}
	}

	//--- Clonable
	// @Override
	clone () {
		return new Circle(this.x, this.y, this.radius);
	}
		// Clone only origin
	cloneOrigin () {
		return super.clone();
	}

	//--- Drawable
	// @Override
	draw (view, color = 'white', style = View.STROKE, lineWidth = 1) {
		super.draw(view, color, this.radius, style, lineWidth);
	}
		// draw only origin
	drawOrigin (view, color, radius, style, lineWidth) {
		super.draw(view, color, radius, style, lineWidth);
	}
}