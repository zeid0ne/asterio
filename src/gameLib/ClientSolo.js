'use strict';

	// Controller (client)
module.exports = global.ClientSolo = class ClientSolo extends AbstractSocketManager  {

		// - game Game : model
		// - view View
	constructor (game, view) {
		super(game, null);

		this.view = view;

			// TODO put color in args
		const colorId = Utility.random(5);

			// Create player and prepare it for generation in game
		this.player = new Player(colorId);
		this.game.entityManager.setUpGenerate(this.player);

			// Fix camera on player
		if (View.WATCH_PLAYER) {
			this.view.watch(this.player);
		}
		
			// Evenements
			// MAYBE add listener on view (view.body.addEventListener...)
		window.addEventListener('keydown'	, e => this.onKeyDown	(e));
		window.addEventListener('keyup'		, e => this.onKeyUp		(e));
		window.addEventListener('mousemove'	, e => this.onMouseMove	(e));
		window.addEventListener('mousedown'	, e => this.onMouseDown	(e));
		window.addEventListener('mouseup'	, e => this.onMouseUp	(e));
	}
		// Keys input
	onKeyDown (e) {
		this.onKey(e, true);
	}
	onKeyUp (e) {
		this.onKey(e, false);
	}
		// Handle keys input : if key is in input config => set activation
	onKey (e, boolean) {
		if (this.player.input.hasKeyCode(e.keyCode)) {
			this.player.input.setKeyActivation(e.keyCode, boolean);
		}
	}

		// Mouse input
	onMouseMove (e) {
		this.player.input.cursor.setPosition(
			e.clientX - this.view.x, 
			e.clientY - this.view.y
		);
		this.player.input.cursor.setActivation(true);
	}
	onMouseDown (e) {
		this.onMouse(true);
	}
	onMouseUp (e) {
		this.onMouse(false);
	}
		// Handle mouse click
	onMouse (boolean) {
		this.player.input.cursor.setDown(boolean);
	}
}