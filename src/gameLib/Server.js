'use strict';

	// Controller (server)
module.exports = global.Server = class Server extends AbstractSocketManager {

		// - game : Game (Model)
		// - socket : io.listen(...).sockets
	constructor (game, socket) {
		super(game, socket);

			// On a client connection
		this.socket.on('connection', this.onClientConnection.bind(this));
	}

		// On a client connection
	onClientConnection (client) {
			// - Creation of the Player entity for the client with a new id
			// TODO args {name, color}
		const player = new Player(Utility.random(5));
		client.id = this.game.setUpEntityGenerate(player);

			// Send id to the client to confirm connection
		client.emit('onConnected', client.id);

			// Send all generation message of the current state of the game
			// MAYBE complex because I had to send the whole game state
			//	We can avoid this by start the game only when all players are in
			// 	But if a player exit the game, he can't  come back
		client.emit('onMessageList', this.game.entityManager.getGenerate());

			// Listeners
			// On client input message reception
		client.on('onInputMessage', this.onInputMessage.bind(this, client));

			// On client disconnection
		client.on('disconnect', this.onClientDisconnection.bind(this, client));
	}
		// On client disconnection
		// TODO define action when player disconnect
	onClientDisconnection (client) {
		this.game.setUpEntityRemove(client.id);
	}

		// Send a message list to all clients
	sendMessageList (messageList) {
		this.sendData('onMessageList', messageList);
	}
		// On client input message reception
		// MAYBE consider time t and broadcast input
		//	This work without this, the syncro difference in imperceptible
	onInputMessage (client, jsonMessage) {
		const message = new Message(
			jsonMessage.type, jsonMessage.subType, jsonMessage.args);
		message.args.id = client.id;
		this.game.messageManager.toHandle.push(message);
		// this.game.messageManager.toSend.push(message);

		if (jsonMessage.subType !== Message.Input.MOUSE_MOVE) {
			// const ms = Date.now() - jsonMessage.args.t;
			// console.log(client.id + ' -> ' + jsonMessage.type + '.' + jsonMessage.subType + '\tms ' + ms);
			// console.log(client.id + ' -> ' + jsonMessage.type + '.' + jsonMessage.subType);
			// console.log(jsonMessage.args);
		}
	}
}