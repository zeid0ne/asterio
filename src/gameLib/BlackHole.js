'use strict';

require('./Hole.js')

module.exports = global.BlackHole = class BlackHole extends Hole {

		// - lifeTime : in seconds
		// - finalRadius = coreRadius + gravityRadius
	constructor (x, y, velocity, lifeTime, finalRadius) {
		super(x, y, 0, 0, true, velocity);
		this.finalRadius = finalRadius;
		this.lifeTime = lifeTime;
		this.time = 0;
		this.growingTime = 0;
			// 1 = growing / -1 = extinction
		this.growingCoef = 1;
		this.isGrowing = true;
		this.isExtinct = false;
	}

	//--- Tangible
	// @Override
	stateUpdate (dt) {
		this.updateTime(dt);
		super.stateUpdate(dt);
	}
	
	updateTime (dt) {
		this.time += dt;
			// Growing or extinction
		if (this.isGrowing) {
			this.growingTime += dt*this.growingCoef;
			this.bound = this.finalRadius*this.growingTime / BlackHole.GROWING_TIME;
			this.coreRadius = this.bound/2;
			this.gravityRadius = this.bound/2;
			this.hitbox.radius = this.bound;
				// Stop
			if (this.growingCoef > 0 && this.growingTime >= BlackHole.GROWING_TIME) {
				this.isGrowing = false;
			} else if (this.growingCoef < 0 && this.growingTime <= 0) {
				this.isGrowing = false;
				this.isExtinct = true;
			}
		}
			// if lifetime is reached : start extinction
		else if (this.time >= this.lifeTime) {
			this.growingTime = BlackHole.GROWING_TIME;
			this.growingCoef = -1;
			this.isGrowing = true;
		}
	}

	//--- Drawable
	// @Override
	draw (view) {
			// Check if is extinct to fix drawing arc with maybe negative radius
		if (!this.isExtinct) {
			super.draw(view);
		}
	}
}