'use strict';

module.exports = global.Bullet = class Bullet extends Entity {

		// - playerId : id from the player who shoot him
		// - colorId : Utility.RED...
		// - shootOrigin : the shoot origin
		// - velocity : Vector
		// - type : Bullet.NORMAL...
	constructor (playerId, colorId, shootOrigin, velocity, type) {
			// MAYBE radius constant
		const hitbox = new Circle(shootOrigin.x, shootOrigin.y, 2);
		super(undefined, hitbox, velocity);

		this.colorId 	= colorId;
		this.playerId 	= playerId;
		this.type		= type;

			// Need to store it for drawing wave bullet
		this.shootOrigin = shootOrigin.clone();

		this.isHitting = false;

			// To make it disapear after certain amount of time
		this.time = 0;
		this.isExtinct = false;
	}
		// When bullet hit other entity
		// If it's FRAG, return other bullets out of the hitbox
	hit (hitbox) {
		this.isHitting = true;
		const list = [];
		if (this.type === Bullet.FRAG) {
			for (let k = -1; k < 2; ++k) {
				const speed = this.velocity.magnitude;
				const direction = this.velocity.direction + k*Math.PI/4 +
					Utility.random(-4, 4) * Math.PI/32;
				const velocity = new Vector(speed, direction);
				const origin = hitbox.cloneOrigin();
				const shift_vector = new Vector(hitbox.pointList[0].range(hitbox)-5, direction);
				origin.shift(shift_vector)
				const bullet = new Bullet(this.playerId, this.colorId,
					origin, velocity, Bullet.TRIPLE);
				list.push(bullet);
			}
		}
		return list;
	}

	// @Override
	stateUpdate (dt) {
		this.updateTime(dt);
		super.stateUpdate(dt);
	}

	updateTime (dt) {
		if (this.time < Bullet.LIFETIME) {
			this.time += dt;
			if (this.time >= Bullet.LIFETIME) {
				this.time = Bullet.LIFETIME;
				this.isExtinct = true;
			}
		}
	}

	//--- Drawable
	draw (view, dt) {
		this.drawPhysic(view);

			// Draw only if bullet is slightly away from the shoot origin
			//  to not draw under the player ship
		if (View.DRAW_VIEW && !this.ghost.hitbox.near(this.shootOrigin, 16)) {
			if (View.DRAW_IMAGE) {
				view.ctx.save();
					// If WAVE : translate on shoot origin to properly draw arc
				if (this.type === Bullet.WAVE) {
					view.ctx.translate(
						this.shootOrigin.x + view.x, this.shootOrigin.y + view.y);
				}
					// Else translate context on bullet 
				else {
					view.ctx.translate(
						this.ghost.hitbox.x + view.x,
						this.ghost.hitbox.y + view.y);
				}
					// And rotate context
				view.ctx.rotate(this.velocity.direction);

					// Calculate length scale according to lifetime
				const length_scale = Bullet.LIFETIME - this.time;

				const yTileset = 3*this.colorId;
				switch (this.type) {
						// Laser : longer
					case Bullet.LASER : {
						view.ctx.drawImage(Utility.BULLET_TILESET, 0, yTileset,
							15, 3, -30, -1, 45*length_scale, 3); break;
					}
						// Wave : arc gradiant around origin
					case Bullet.WAVE : {
						const range = this.shootOrigin.range(this.ghost.hitbox);
						const angle = (Math.PI/36) / (range/100);
						view.ctx.lineWidth = .5 + 3.5*length_scale;
							// The gradiant is composed by 3 arcs
						for (let i = 0; i < 3; i++) {
							const shade = (2 - i)*50;	// -50, 0 , +50
							view.ctx.strokeStyle = 
								Utility.getRgbaColor(this.colorId, shade);
							view.ctx.beginPath();
								// Try to avoid arc of negative length
							try {
								view.ctx.arc(0, 0, range - i*3, -angle, angle);
							} catch (e) {}
							view.ctx.stroke();
						} break;
					}
						// Triple : thinner
					case Bullet.TRIPLE : {
						view.ctx.drawImage(Utility.BULLET_TILESET, 0, yTileset,
							15, 2, -7.5, -1, 15*length_scale, 2); break;
					}
						// Homing : shorter and oriented to the targeted asteroid
					case Bullet.HOMING : {
						view.ctx.drawImage(Utility.BULLET_TILESET, 0, yTileset,
							15, 3, -5, -1, 10*length_scale, 3); break;
					}
						// Frag : larger
					case Bullet.FRAG : {
						for (let i = -1; i < 2; i++) {
							view.ctx.drawImage(Utility.BULLET_TILESET, 0, yTileset,
								15, 3, -6, i*2 - 1, 12*length_scale, 3);
						} break;
					}
						// Normal
					default : {
						view.ctx.drawImage(Utility.BULLET_TILESET, 0, yTileset,
							15, 3, -7.5, -1, 15*length_scale, 3);
					}
				}
				view.ctx.restore();
			} else {
				this.ghost.hitbox.draw(
					view, Utility.getRgbaColor(this.colorId, 150), View.FILL);
			}
		}
	}
}