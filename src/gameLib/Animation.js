'use strict';

module.exports = global.Animation = class Animation {
    
        // - id : Blast, Trace...
        // - watched : the center point of the animation, can move
        // - args : color, time over etc...
    constructor (id, watched, args) {
        this.id = id;
        this.watched = watched;
        this.args = args;
            // Milliseconds
        this.time = 0;
            // True when the animation must be removed from view
        this.isOver = false;
    }

    draw (view, dt) {
            // Time flows
        this.time += dt;

        switch (this.id) {
                // Growing faded circle around the impact
            case Animation.BLAST : {
                this.watched.draw(view, 
                    Utility.getRgbaColor(this.args.colorId, 10*Utility.random(5, 17)),
                    this.time*5, View.STROKE,
                    (this.args.timeOver - this.time)*2);
                if (this.time > this.args.timeOver) {
                    this.isOver = true;
                }
                break;
            }
                // Triangles likes fragments
			case Animation.ASTEROID_BREAK : {
				const triangle = new Polygon(
                    [0,  1 - this.args.timeOver, this.args.timeOver - 1],
					[0,  6 + this.args.timeOver, 6 + this.args.timeOver], this.watched);
                triangle.shift(0, 4*this.time);
                triangle.pivot(this.watched, this.args.orientation);
                for (let i = 0; i < 5; i++) {
                        // Pivot the triangle
                    triangle.pivot(this.watched, Math.PI2/5);
                        // Color, shade according to triangle orientation
					const shade = 50 + Math.floor(Utility.toDeg(
                        Math.abs(triangle.pointList[0].angle(triangle.pointList[1]))
                    )/2);
                    const color = Utility.getRgbaColor(this.args.colorId, shade);
                        // Draw
                    triangle.draw(view, color, View.FILL);
					triangle.draw(view, 'black', View.STROKE);
                }
                if (this.time > this.args.timeOver) {
                    this.isOver = true;
                }
                break;
			}
        }
    }
}