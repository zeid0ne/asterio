'use strict';

module.exports = global.Point = class Point {

		// Default on (0,0)
	constructor (x = 0, y = 0) {
		this.x = x;
		this.y = y;
	}
		// Return the angle (in radian) with the point
	angle (point) {
		return Math.atan2(point.y - this.y, point.x - this.x);
	}
		// Return the distance (in pixel) between the point
	range (point) {
		return Math.sqrt(
			Math.pow(point.x - this.x, 2) + 
			Math.pow(point.y - this.y, 2));
	}
		// TRUE if the distance between the points are less than the range
	near (point, range) {
		return (this.range(point) <= range);
	}

	//--- Movable
		// Set x & y
	setPosition (x, y) {
		this.x = x;
		this.y = y;
	}
		// Translation (in pixel)
		// - vx : Vector or Point
	shift (vx, vy) {
		if (vx instanceof Point) {
			this.x += vx.x;
			this.y += vx.y;
		} else {
			this.x += vx;
			this.y += vy;
		}
	}
		// Rotate by angle (in radian) around the origin (Point)
	pivot (origin, angle) {
		const cos = Math.cos(angle);
		const sin = Math.sin(angle);
		const x2 = this.x - origin.x;
		const y2 = this.y - origin.y;
		this.x = cos*x2 - sin*y2 + origin.x, 
		this.y = sin*x2 + cos*y2 + origin.y;
	}

	//--- Clonable
	clone () {
		return new Point(this.x, this.y);
	}

	//--- Drawable
		// Draw a circle or a disc
	draw (view, color = 'white', radius = 5, style = View.FILL, lineWidth = 1) {
		view.ctx.lineWidth = lineWidth;
		view.ctx[style + 'Style'] = color;
		view.ctx.beginPath();
		view.ctx.arc(this.x + view.x, this.y + view.y, radius, 0, Math.PI2);
		view.ctx[style]();
	}

		// Draw dashed
	drawPhysic (view, color) {
		if (View.DRAW_PHYSIC) {
			view.ctx.setLineDash([1, 4]);
			this.draw(view, color, View.STROKE);
			view.ctx.setLineDash([]);
		}
	}
}