'use strict';

	// Controller (client)
module.exports = global.Client = class Client extends AbstractSocketManager {

		// - game Game : Model
		// - socket : io.connect()
	constructor (game, view, socket) {
		super(game, socket);

		this.view = view;

			// On server connection
		this.socket.on('onConnected', this.onConnected.bind(this));

			// On message reception
		this.socket.on('onMessageList', this.onMessageList.bind(this));

			// Evenements
		window.addEventListener('keydown'	, e => this.onKeyDown	(e));
		window.addEventListener('keyup'		, e => this.onKeyUp		(e));
		window.addEventListener('mousemove'	, e => this.onMouseMove	(e));
		window.addEventListener('mousedown'	, e => this.onMouseDown	(e));
		window.addEventListener('mouseup'	, e => this.onMouseUp	(e));
	}

		// On connection confirmation
	onConnected (id) {
		this.id = id;
		console.log('Player id ' + id);
	}

		// Keys input
	onKeyDown (e) {
		this.onKey(e, true, Message.Input.KEY_DOWN);
	}
	onKeyUp (e) {
		this.onKey(e, false, Message.Input.KEY_UP);
	}
		// Handle keys input : if key is in input config => set activation
	onKey (e, isDown, messageType) {
		const player = this.game.entityManager.idMap.get(this.id);
		if (player.input.hasKeyCode(e.keyCode)
			&& player.input.keyMap.get(e.keyCode).isActive !== isDown) {
			
			player.input.setKeyActivation(e.keyCode, isDown);

			const keyName = player.input.keyMap.get(e.keyCode).name;
			this.sendInputMessage(messageType, {keyName : keyName});
		}
	}

		// Mouse input
	onMouseMove (e) {
		const x = e.clientX - this.view.x;
		const y = e.clientY - this.view.y;
		
		this.sendInputMessage(Message.Input.MOUSE_MOVE, { x : x, y : y });

		// const player = this.game.entityManager.idMap.get(this.id);
		// player.input.cursor.setPosition(x, y);
		// player.input.cursor.setActivation(true);
	}
	onMouseDown (e) {
			// CSP Generate bullets on client side to display prediction
		/*
		const player = this.game.entityManager.idMap.get(this.id);
		player.input.cursor.setDown(true);
		const bulletList = player.shoot();
		const idList = [];
		for (let bullet of bulletList) {
			this.game.setUpEntityGenerate(bullet);
			bullet.colorId = Utility.BLUE;
			bullet.id = -bullet.id;
			idList.push(bullet.id);
		}
		*/
		// this.sendInputMessage(Message.Input.MOUSE_DOWN, { idList: idList });
			//---

		this.sendInputMessage(Message.Input.MOUSE_DOWN);
	}
	onMouseUp (e) {
		this.sendInputMessage(Message.Input.MOUSE_UP);
		// try {
			// const player = this.game.entityManager.idMap.get(this.id);
			// player.input.cursor.setDown(false);
		// } catch (e) {
			// console.log(this.id);
			// console.log(this.game.entityManager.idMap);
		// }
	}

		// Send input message to server
	sendInputMessage (subType, args = {}) {
			// CSP Time of the interaction	
		// args.t = Date.now();

		const message = new Message(Message.Type.INPUT, subType, args);
		this.sendData('onInputMessage', message);
	}

		// On message reception from server
	onMessageList (messageList) {
		for (let jsonMessage of messageList) {
			const message = new Message(
				jsonMessage.type, jsonMessage.subType, jsonMessage.args);
			this.game.messageManager.toHandle.push(message);
		}
	}

}