'use strict';

require('./Utility.js'	);
require('./View.js'		);
require('./Key'			);
require('./Star.js'		);
require('./Asteroid.js'	);
require('./Game.js'		);
require('./Space.js'	);
require('./Polygon.js'	);
require('./Player.js'	);
require('./Gun.js'		);
require('./GunConfig.js');


//--- Math & Point
Math.PI2 	= Math.PI * 2;
Math.PI_2 	= Math.PI / 2;
	// To avoid to create new object each time for basic calculs
Point.ZERO = new Point();


//--- Color (in Utility)
	// According to tileset position
Utility.RED 	= 0
Utility.GREEN 	= 1;
Utility.YELLOW	= 2;
Utility.BLUE	= 3;
Utility.PURPLE	= 4;
Utility.ORANGE	= 5;
Utility.GRAY	= 6;

	// Rgb addition to tint the color
Utility.RGB_MAP = new Map();
Utility.RGB_MAP.set(Utility.GRAY	, [0 , 0 , 0 ]);
Utility.RGB_MAP.set(Utility.RED		, [50, 0 , 0 ]);
Utility.RGB_MAP.set(Utility.BLUE	, [0 , 50, 50]);
Utility.RGB_MAP.set(Utility.GREEN	, [0 , 50, 0 ]);
Utility.RGB_MAP.set(Utility.YELLOW	, [50, 50, 0 ]);
Utility.RGB_MAP.set(Utility.ORANGE	, [50, 0 ,-25]);
Utility.RGB_MAP.set(Utility.PURPLE	, [50, 0 , 50]);


//--- Player
	// Distance pixel/second, rotation angle/second
Player.ACCELERATION	= 200;
Player.SPEED_MAX	= 200;
Player.ROTATION		= Math.PI2;
Player.DRAG			= .96;
Player.BOUND	= 25;
Player.HITBOX 	= new Polygon(
	[ 16, -13, -15, -15, -13,  16],
	[  2,  16,  16, -15, -15,  -1]);
// WARNING : build polygon in anti clockwise : 0 => Math.PI2


//--- Bullet
Bullet.SPEED 	= 500;
Bullet.NORMAL 	= 0;
Bullet.FRAG		= 1;
Bullet.HOMING	= 2;
Bullet.WAVE		= 3;
Bullet.TRIPLE	= 4;
Bullet.LASER	= 5;

Bullet.HOMING_RANGE = 200;
Bullet.LIFETIME = 1;	// In second


//--- Gun
Gun.CADENCE = 5;		// bullet/second
Gun.TIME_CONFIG = 10;	// seconds before gun reset to normal


//--- GunConfig
	// According to ship position in tileset
GunConfig.NORMAL 	= 0
GunConfig.DOUBLE 	= 1
GunConfig.LASER 	= 2
GunConfig.WAVE 		= 3
GunConfig.TRIPLE 	= 4
GunConfig.HOMING 	= 5
GunConfig.FRAG 		= 6

GunConfig.MAP = new Map();
GunConfig.MAP.set(GunConfig.NORMAL	, new GunConfig(1	, Bullet.NORMAL	, Utility.GRAY	));
GunConfig.MAP.set(GunConfig.DOUBLE 	, new GunConfig(1.5	, Bullet.NORMAL	, Utility.RED	));
GunConfig.MAP.set(GunConfig.LASER 	, new GunConfig(.75	, Bullet.LASER	, Utility.GREEN	));
GunConfig.MAP.set(GunConfig.WAVE 	, new GunConfig(.75	, Bullet.WAVE	, Utility.YELLOW));
GunConfig.MAP.set(GunConfig.TRIPLE 	, new GunConfig(1	, Bullet.TRIPLE	, Utility.BLUE	));
GunConfig.MAP.set(GunConfig.HOMING 	, new GunConfig(1	, Bullet.HOMING	, Utility.PURPLE));
GunConfig.MAP.set(GunConfig.FRAG 	, new GunConfig(.75	, Bullet.FRAG	, Utility.ORANGE));

//--- Asteroid
Asteroid.SIZE_MAX 		= 4;
Asteroid.RADIUS_COEF	= 10;
Asteroid.RADIUS_MIN		= 10;
Asteroid.NB_SIDE_MIN 	= 5;
Asteroid.NB_SIDE_MAX 	= 7;

Asteroid.SPEED_COEF = Player.SPEED_MAX / Asteroid.SIZE_MAX;
//Asteroid.AREA		= 
//	Math.PI*(Asteroid.RADIUS_MIN + Asteroid.RADIUS_COEF*Asteroid.SIZE_MAX)**2;


//--- BlackHole
BlackHole.SPEED_MIN		= 25;
BlackHole.SPEED_MAX		= 50;
BlackHole.RADIUS_MIN	= 200;
BlackHole.RADIUS_MAX	= 400;
// radius = coreRadius + gravityRadius & coreRadius = gravityRadius

BlackHole.GROWING_TIME = 2;	// seconds to before radius = final radius
BlackHole.MIN_LIFETIME = 10 // lifetime  before extinction
BlackHole.MAX_LIFETIME = 20;


//--- Space
Space.CORE_RADIUS		= 700;
Space.GRAVITY_RADIUS	= 100;
Space.DEPTH				= 200;

Space.RADIUS = Space.CORE_RADIUS + Space.GRAVITY_RADIUS;
//Space.AREA		= Math.PI * Space.RADIUS**2;

//--- Star
Star.RADIUS_MIN 	= .25;
Star.RADIUS_MAX 	= 1.5;

Star.RADIUS_DELTA 	= Star.RADIUS_MAX - Star.RADIUS_MIN;
Star.DEPTH_MIN 		= Space.DEPTH*.5;
Star.DEPTH_MAX 		= Space.DEPTH;
Star.DEPTH_DELTA 	= Star.DEPTH_MAX - Star.DEPTH_MIN;


//--- Animation
Animation.BLAST				= 0;
Animation.ASTEROID_BREAK 	= 1;


//--- View
View.FILL 	= 'fill';
View.STROKE	= 'stroke';
View.ORDER_DRAW = [BlackHole, Asteroid, Player, Bullet];

View.STAR_FREQ		= 0.0002;
View.STAR_DENSITY 	= 1 / View.STAR_FREQ;

View.WATCH_PLAYER 	= true;
View.DRAW_STAR		= true;
View.DRAW_IMAGE		= true;
View.DRAW_VIEW		= true;
View.DRAW_ANIMATION	= true;
// View.DRAW_HITBOX	= true;
// View.DRAW_BOUND		= true;
// View.DRAW_VELOCITY 	= true;
// View.DRAW_TRACE		= .0;	// 0 for full trace or .05 for cool blur


//--- Game
Game.UPS 	= 30;
Game.MS_UPS	= 1000/Game.UPS;

Game.NB_ASTEROID = 50;
Game.BONUS_FREQ = 8;		// 8 nbBonus*freq
// Game.PLAYER_LOSE_GUN = true;	// lose gun when hit player asteroid
// Game.PLAYER_KEEP_GUN = true;	// keep gun for the game
Game.NB_BLACKHOLE = 0;