'use strict';

	// Abstract controller
module.exports = global.AbstractSocketManager = class AbstractSocketManager {

		// - game Game	: Model instance
		// - socket 	: source connection (server or client)
	constructor (game, socket) {
		this.game 	= game;
		this.socket = socket;
		this.game.setSocketManager(this);
	}
		// Envoi un message
	sendData (messageClass, message) {
		this.socket.emit(messageClass, message);
	}
}