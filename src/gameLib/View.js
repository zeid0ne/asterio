'use strict';

require('./Point.js');
	
	// MAYBE Point heritage is useless, maybe usefull to make angle later ?
module.exports = global.View = class View extends Point {

	constructor (game) {
		super();

		this.game = game;

			// Time in milliseconds
		this.time;

			// Get html
		this.body 	= window.document.querySelector('body');
		this.canvas = this.body.querySelector('canvas');
		this.canvas.center = new Point();
		this.ctx = this.canvas.getContext('2d', {alpha : false});
		// this.ctx.font = '15px DejaVu sans Mono';

			// Point always in center of the screen (default = client player)
		this.watched = new Point();

			// Generate stars in space
		this.setNewStarList();

			// Animations
		this.animationList = [];

			// Set canvas width / height / center
		this.resize();
		window.addEventListener('resize', this.resize.bind(this));
	}
		// Recalculate canvas width / height / center
	resize () {
		this.canvas.width	= this.body.getBoundingClientRect().width;
		this.canvas.height	= this.body.getBoundingClientRect().height;
		this.canvas.center.x = this.canvas.width	/ 2;
		this.canvas.center.y = this.canvas.height	/ 2;
	}
		// Define the object always in center of screen
		// - object : Point or Entity
	watch (object) {
		this.watched = object;
	}
		// Draw engine
	start () {
			// Start the render loop
		this.render();
	}
		// Draw engine called each FPS
	render (timestamp) {

			// TODO use timestamp, need to scale units with game.time
		this.time = Date.now();

			// Delta between draw engine and game engine
		const dt = (this.time - this.game.time) / this.game.frameTime || 0;

			// Clear screen
		this.clear();

			// Update ghost entities (hitbox to draw) according to delta time
		for (let [id, entity] of this.game.entityManager.get()) {
			entity.ghostUpdate(dt);
		}

			// Update center referential based on the watched object
		this.updateReferential();

			// Draw game (space, stars, entities, animations)
		this.game.space.draw(this);
		if (View.DRAW_STAR) {
			for (let star of this.starList) {
				star.draw(this, this.game.space,
					this.game.entityManager.get(BlackHole));
			}
		}
		for (let type of View.ORDER_DRAW) {
			const entityMap = this.game.entityManager.get(type);
				// Reverse order to display last entities under first entities
			const entityList = [...entityMap.values()].reverse();
			for (let entity of entityList) {
				entity.draw(this, dt);
			}
		}
			// MAYBE map for animation type
		for (let animation of this.animationList) {
			animation.draw(this, dt);
		}
			// Remove over animations
		this.animationList = this.animationList.filter(animation => !animation.isOver);

			// Info display on realtime to debug
		// const ms_render = Date.now() - this.time;
		// const fps_render = parseInt(1000 / ms_render);

		// this.min_ms_render = Math.min(this.min_ms_render, ms_render) || 100;
		// this.max_ms_render = Math.max(this.max_ms_render, ms_render) || 0;
		// this.min_fps_render = Math.min(this.min_fps_render, fps_render) || 100;
		// this.max_fps_render = Math.max(this.max_fps_render, fps_render) || 0;

		let player;
		if (!this.game.isSolo) {
			player = this.game.entityManager.idMap.get(this.game.socketManager.id);
		} else {
			player = this.game.entityManager.idMap.get(0);
		}
		if (player) {
			const debugInfoList = [
				// 'View render ms  ' + ms_render,
				// '            min ' + this.min_ms_render,
				// '            max ' + this.max_ms_render,
				// 'View render fps ' + fps_render,
				// '            min ' + this.min_fps_render,
				// '            max ' + this.max_fps_render,
				// 'Delta time ' + dt,
				// // 'Player x ' + parseInt(player.hitbox.x),
				// 'Player y ' + parseInt(this.game.socketManager.player.hitbox.y),
				// 'Player v ' + parseInt(player.velocity.magnitude),
				// 'Asteroids ' + this.game.entityManager.get(Asteroid).size,
				// 'Bonus ' + this.game.entityManager.nbBonus,
				// 'this.time ' + this.time,
				// 'timestamp ' + timestamp
			];
			this.ctx.fillStyle = 'lime';
			this.ctx.font = '15px monospace';
			for (let i = 0; i < debugInfoList.length; i++) {
				this.ctx.fillText(debugInfoList[i], 50, 50 + i*30);
			}
		}

			// Schedule the next frame
		requestAnimationFrame(this.render.bind(this));
	}
		// Clear screen
	clear () {
		if (View.DRAW_TRACE === undefined) {
			// this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
			this.ctx.fillStyle = 'rgba(0, 0, 0, 1)';
			this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
		} else {
				// Apply black filter with transparency
			this.ctx.fillStyle = 'rgba(0, 0, 0, ' + View.DRAW_TRACE + ')';
			this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
		}
	}
		// Update referential according to the position of the watched object
		//  and according to the window size
	updateReferential () {
		if (this.watched.ghost) {
			this.x = this.canvas.center.x - this.watched.ghost.hitbox.x;
			this.y = this.canvas.center.y - this.watched.ghost.hitbox.y;
		} else if (this.watched instanceof Entity) {
			this.x = this.canvas.center.x - this.watched.hitbox.x;
			this.y = this.canvas.center.y - this.watched.hitbox.y;
		} else if (this.watched instanceof Point) {
			this.x = this.canvas.center.x - this.watched.x;
			this.y = this.canvas.center.y - this.watched.y;
		} else {
			this.x = this.canvas.center.x;
			this.y = this.canvas.center.y;
		}
	}
		// Fill the Stars list
	setNewStarList () {
		this.starList = [];
		const radius = this.game.space.coreRadius;
		for (let x = -radius; x < radius; ++x) {
			for (let y = -radius; y < radius; ++y) {
				if (!Utility.random(View.STAR_DENSITY)) {
					const vector = new Vector(x, y, true);
					if (vector.magnitude <= radius) {
						const depth = Utility.random(
							Star.DEPTH_MIN, Star.DEPTH_MAX);
						const star = new Star(vector.x, vector.y, depth);
						this.starList.push(star);
					}
				}
			}
		}
	}
		// Setup the animation from Game.js before entity is removed
	setUpAnimation (entity, animationId) {
			// Initialize the animation
		const animation = new Animation(animationId);
		switch (animationId) {
				// Bullet destruction
			case Animation.BLAST : {
				animation.watched = entity.hitbox.cloneOrigin();
				animation.args =  {
					timeOver : Utility.random(3, 4),
					colorId : entity.colorId
				};
				break;
			}
				// Asteroid destruction
			case Animation.ASTEROID_BREAK : {
				animation.watched = entity.hitbox.cloneOrigin();
				animation.args = {
					timeOver : 4 + entity.size,
					colorId : entity.getColorId(),
					orientation : Utility.toRad(Utility.random(0, 7)*10)
				};
				break;
			}
		}
			// Add it to the animation list and remove it from the entity
		this.animationList.push(animation);
	}

		// To debug
	text (text, point) {
		this.ctx.fillStyle = 'lime';
		this.ctx.font = '10px monospace';
		this.ctx.fillText(text, this.x + point.x, this.y + point.y);
	}
}