'use strict';

module.exports = global.EntityManager = class EntityManager {

	constructor () {
			// Iterator for entities id
		this.nextId = 0;

			// All entitites
		this.idMap = new Map();

			// Map of entities by type (Asteroid, Player...) in draw order
		this.typeMap = new Map();
		for (let type of View.ORDER_DRAW) {
			this.typeMap.set(type.name, new Map());			
		}

			// Queues
		this.toGenerate = [];
		this.toRemove = [];

		this.nbBonus = 0;
	}
		// Return a map of all typed entities in args
		// - object : class entity
	get (...objectConstructorList) {
			// If there is no type, return the map of all types
		if (objectConstructorList.length === 0) {
			return this.idMap;
		}
			// If only one type the function is simple
		if (objectConstructorList.length === 1) {
			return this.typeMap.get(objectConstructorList[0].name);
		}
			// Else we have to set a new map dynamically
			// MAYBE refactor
		const newMap = new Map();
		for (let objectConstructor of objectConstructorList) {
			const idMap = this.typeMap.get(objectConstructor.name);
			for (let [id, entity] of idMap) {
				newMap.set(id, entity);
			}
		}
		return newMap;
	}
		// Return a map of all entities except typed entities in args
		// - object : class entity
		// MAYBE usefull, keep here for now
	/*
	getNot (...objectConstructorList) {
			// Si aucune map exclue
		if (objectConstructorList.length === 0) {
			return this.idMap;
		}
		var newMap = new Map();
		var keyList = [];
		for (var objectConstructor of objectConstructorList) {
			keyList.push(objectConstructor.name);
		}
		for (var [key, idMap] of this.typeMap) {
			if (!keyList.includes(key)) {
				for (var [id, entity] of idMap) {
					newMap.set(id, entity);
				}
			}
		}
		return newMap;
	}
	*/
		// Set an id to the entity add add it to the generate list
		//	then return the generated id
		// - entity : Entity
	setUpGenerate (entity) {
		// console.log('next id ' + this.nextId);
			// set and increment id
		entity.id = this.nextId++;
		
			// Add in generate queue
		this.toGenerate.push(entity);

		return entity.id;
	}
		// Add entity in remove queue
	setUpRemove (id) {
		this.toRemove.push(id);
	}
		// Move entity in generate queue in active queue
	handleGenerate () {
		for (let entity of this.toGenerate) {
			this.idMap.set(entity.id, entity);
			this.typeMap.get(entity.constructor.name).set(entity.id, entity);
			this.handleNbBonus(entity, 1);
		}
		this.toGenerate = [];
	}
		// Destroy (erase from memory) all entities in remove queue
	handleRemove () {
		for (let id of this.toRemove) {
			this.handleNbBonus(this.idMap.get(id), -1);
			this.idMap.delete(id);
			for (let [type, idMap] of this.typeMap) {
				idMap.delete(id);
			}
		}
		this.toRemove = [];
	}
		// Count nb bonus
	handleNbBonus (entity, value) {
		if (entity instanceof Asteroid) {
			if (entity.isBonus()) {
				this.nbBonus += value;
			}
		}
	}

	getGenerate () {
		const messageList = [];
		for (let [id, entity] of this.idMap) {
			const message = new Message(
				Message.Type.STATE,
				Message.State.GENERATE,
				{
					type : entity.constructor.name,
					json : Serializable.toJson(entity),
					// t : Date.now()
				}
			);
			messageList.push(message);
		}
		return messageList;
	}
}