'use strict';

	// Message client / server (entities management, player inputs...)
module.exports = global.Message = class Message {
	
		// - type : Message.Type
		// - subType :
		// 		- INPUT	: Message.Input (KEY_DOWN...)
		// 		- STATE	: Message.State	(GENERATE...)
		// - args :
		// 		- GENERATE 	: Message.Entity + json
		// 		- REMOVE	: id
		// 		- STATE		: info d'affichage (id, x, y, orientation...)
		// 			principalement pour les joueurs, car les autres entités
		// 			(sont prédictibles => comportement défini à la génération)
		// 		- INPUT		: Message.Input + infos de l'intéraction
	constructor (type, subType, args) {
		this.type		= type;
		this.subType	= subType;
		this.args		= args;
	}
}

	// MAYBE remove subtypes and make 8 types
Message.Type = {
	INPUT	: 'i',
	STATE	: 's',
};

	// Subtype player input (client to server)
Message.Input = {
	KEY_DOWN	: 'kd',
	KEY_UP		: 'ku',
	MOUSE_MOVE	: 'mm',
	MOUSE_DOWN	: 'md',
	MOUSE_UP	: 'mu',
};
	// Subtype state of entity (server to clients)
Message.State = {
	GENERATE	: 'g',
	UPDATE		: 'u',
	REMOVE		: 'r',
};