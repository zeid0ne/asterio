'use strict';

module.exports = global.Hole = class Hole extends Entity {

		// - coreRadius : inner
		// - gravityRadius : each entity in this range would be affected
		// - isRepulsing : TRUE = repulse, FALSE = attract
	constructor(x, y, coreRadius, gravityRadius, isRepulsing, velocity) {
		const bound = coreRadius + gravityRadius;
		const hitbox = new Circle(x, y, bound);
		super(bound, hitbox, velocity);
		this.coreRadius 	= coreRadius;
		this.gravityRadius 	= gravityRadius;
		this.isRepulsing 	= isRepulsing;
	}
		// TRUE if the entity histbox is in the gravity radius
	isInGravityRadius (entity) {
		const range = this.hitbox.range(entity.hitbox);
		return this.coreRadius < range && range < this.hitbox.radius;
	}
		// TRUE if the point is in the core radius
	isInCoreRadius (point) {
		return this.hitbox.near(point, this.coreRadius);
	}
		// Apply a force on entity
	applyForce (entity, dt) {
			// Calculate direction and force
		const range = this.hitbox.range(entity.hitbox);
		let deltaRadius;
		let direction;
		if (this.isRepulsing) {
			deltaRadius = this.hitbox.radius - range;
			direction 	= this.hitbox.angle(entity.hitbox);
		} else {
			deltaRadius	= range - this.coreRadius;
			direction 	= entity.hitbox.angle(this.hitbox);
		}

		const penetrationCoef = deltaRadius / this.gravityRadius;

			// Add +1 otherwise the entity can pass trought the hole, but why ?
		const gravityForce = 1 + entity.velocity.magnitude;

		const speed = gravityForce * penetrationCoef
		const acceleration = speed / dt;
		const force = new Vector(acceleration, direction);
		entity.addForce(force);
	}

	//--- Drawable
	// @Override
	draw (view) {
		this.drawPhysic(view);

			// Draw the core circle if the star drawing is disabled
		if (View.DRAW_VIEW && !View.DRAW_STAR) {
			const core = new Circle(this.hitbox.x, this.hitbox.y, this.coreRadius);
			core.draw(view);
			this.hitbox.draw(view);
		}
	}

	drawPhysic (view) {
		super.drawPhysic(view);
			// Draw the core circle dashed
		if (View.DRAW_HITBOX) {
			const core = new Circle(this.hitbox.x, this.hitbox.y, this.coreRadius);
			view.ctx.setLineDash([1, 4]);
			core.draw(view);
			view.ctx.setLineDash([]);
		}
	}
		// Draw a deformed star in gravity radius
	drawStarInGravityRadius (view, star, color, radius) {
			// Direction and range with the ghost point (see Star.js)
		const direction 	= this.hitbox.angle(star.ghostPoint);
		const range 		= this.hitbox.range(star.ghostPoint);

			// Calculate deformation according to repulsion boolean
		var penetrationCoef;
		if (this.isRepulsing) {
				// Crush stars on core radius
			penetrationCoef = range / this.hitbox.radius;
		} else {
				// Keep stars in core radius
			penetrationCoef = 1 - (this.coreRadius / range);
			penetrationCoef *= 2;
		}
		const vector = new Vector(
			this.gravityRadius * penetrationCoef + this.coreRadius, direction);
		
			// Set visible star
		var newRangeCoef;
		if (this.isRepulsing) {
			newRangeCoef = vector.magnitude / (this.hitbox.radius + 10);
		} else {
			newRangeCoef = this.coreRadius / (vector.magnitude + 10);
		}
		const angle = Math.PI/16 * (1 - newRangeCoef);

		view.ctx.strokeStyle = color;
		view.ctx.lineWidth = radius;
		view.ctx.beginPath();
		view.ctx.arc(
			this.hitbox.x + view.x, this.hitbox.y + view.y, 
			vector.magnitude, direction - angle, direction + angle);
		view.ctx.stroke();
	}
}