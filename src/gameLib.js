const fs 		= require('fs');
const fileList 	= fs.readdirSync(__dirname + '/gameLib/');

	// Export all gamelLib files
for (var file of fileList) {
	module.exports[file.replace('.js', '')] = require('./gameLib/' + file);
}
