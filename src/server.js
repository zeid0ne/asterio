'use strict';

//--- Server : backend process
	// Port
const gamePort = process.env.PORT || 4004;
	// Library
const io		= require('socket.io');
const express	= require('express');
const http		= require('http');
const app		= express();
const server	= http.createServer(app);

//--- Express : route gesture
server.listen(gamePort);
console.log('\nServer OK : port ' + gamePort);

app.use(express.static(__dirname + '/../public'));

	// Default route (/index.html)
app.get('/', function (req, res) {
		// Send index.html to client
	res.sendFile('/index.html', { root: __dirname + '/../public' });
});

//--- Socket.io : connections gesture
	// Instance for express
const sio = io.listen(server);

//--- Game
	// All gameLib file
require('./gameLib.js');
	// Model
const game = new Game();
	// Controller
const gameServer = new Server(game, sio.sockets);
game.setSocketManager(gameServer);
	// Start game
	// TODO start when all player are ok
game.start();