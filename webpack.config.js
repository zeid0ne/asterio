const path = require('path'	);

function config(isSolo = false) {
	const entryFileName 	= isSolo ? './src/clientSolo.js' : './src/client.js'
	const outputFileName	= isSolo ? 'bundleSolo.js' : 'bundle.js'
	return {
		entry: entryFileName,
		output: {
			filename	: outputFileName,
			path		: path.resolve(__dirname, './public')
		},
		mode : 'development',
		node : {
			fs: 'empty'
		},
		module : {
			rules : [
				{
					test : /\.(png)$/,
					use : ['file-loader']
				}
			]
		}
	}
}

module.exports = env => {
	return [config(), config(true)];
}

// webpack --watch
// webpack --w