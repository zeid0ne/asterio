/* Hiérarchie des méthode de classe *//*

Constructeur
Chainage de prototype (héritage)
Setteur
Getteur (attribut ou état)
Modifieur
Rafraichissement (automatique, qui dépendent des attributs)
Intéraction homme - machine
Affichage

*//* Améliorations *//*

Retester for (var i = 0, c = tab.length; i < c; i++)
Tester les performances avec forEach => c'est naze

Image composite pour les couleurs des vaisseaux (vraiment pas prioritaire, car peut ralentir l'affichage)

Classement final par rapport au score max et au nombre de collisions

Interface de connexion (nom, couleur)
Interface options (détail graphisme, animation activé)

Changer le input clavier direction x et y

Classement visible par une nuance dans la timeline

Limites hors map
Apparition et disparition des astéroïdes qui dépassent les limites
Perte de points quand on sort de la map
	=> tourelles qui tirents sur tout ce qui sort, directement sur les astéroïdes, mais avec quelques secondes sur les vaiseaux

/* ----------- */

window.onload = function()
{
/* Test d'ouverture du jeu */
	var body = document.querySelector('#body');

	var canvas = document.querySelector('#canvas');
	if (!canvas) {
		alert('Impossible de récupérer le canvas');
		return;
	}

	var ctx = canvas.getContext('2d');
	if (!ctx) {
		alert('Impossible de récupérer le ctx du canvas');
		return;
	}

/* Chargement des images */
	var tileset_ship = new Image();
	tileset_ship.src = 'img/tileset_ship.png';
	var tileset_bullet = new Image();
	tileset_bullet.src = 'img/tileset_bullet.png';
	
//	var tileset_ship3 = new Image();
//	tileset_ship3.src = 'img/tileset_ship3.png';
//	var tileset_color = new Image();
//	tileset_color.src = 'img/tileset_color.png';

/* Fonctions universelles */
	// Truc qui gère les fps je sais pas trop comment
	var fps =
	{
		start_time : 0,
		frame_number : 0,
		getFPS : function() {
			this.frame_number++;
			var d = new Date().getTime(),
			current_time = (d - this.start_time)/1000,
			result = Math.ceil((this.frame_number/current_time));
			if (current_time > 1)
			{
				this.start_time = new Date().getTime();
				this.frame_number = 0;
			}
			return result;
		},
	};

	// Retourne un entier entre min et max inclus
	function random(min, max)
	{
		var min = Math.ceil(min);
		var max = Math.floor(max);
		return Math.floor(Math.random()*(max - min + 1)) + min;
	}
	// Retourne l'angle deg (en degré) en radian
	function toRad(deg) {
		return deg*PI/180;
	}
	// Retourne l'angle rad (en radian) en degré
	function toDeg(rad) {
		return rad*180/PI;
	}
	// Retourne une chaîne de caractères 
	// correspondante à un numéro de couleur et une nuance
	function getFillColor(color, shade, transparent) {
		var string = (transparent) ? 'rgba(' : 'rgb(';
		switch (color)
		{
			case 0	: string += (shade+50) + ',' + shade + ',' + shade; break;
			case 1	: string += shade + ',' + (shade+50) + ',' + shade; break;
			case 2	: string += (shade+50) + ',' + (shade+50) + ',' + shade; break;
			case 3	: string += shade + ',' + (shade+50) + ',' + (shade+50); break;
			case 4	: string += (shade+50) + ',' + shade + ',' + (shade+50); break;
			case 5	: string += (shade+50) + ',' + shade + ',' + (shade-25); break;
			case 6	: string += (shade+75) + ',' + (shade+75) + ',' + (shade+75); break;
			default	: string += shade + ',' + shade + ',' + shade;
		}
		return (transparent) ? string + ',' + transparent + ')' : string + ')';
	}
	// Retourne une nuance en fonction de l'orientation de la lumière
//	function getShade(light, direction, origin) {
//		var gray = 10 + Math.floor(toDeg(
//			Math.abs(this.origin.angle(this.hitbox[i])) +
//			Math.abs(this.origin.angle(this.hitbox[i2])))/3);
//	}

/* Point */
	// x peut être un Point
	// Si aucun arguments n'est spécifié, cela créer le point (0,0)
	function Point(x, y) {
		if (x instanceof Point) {
			this.x = x.x;
			this.y = x.y;
		}
		else {
			this.x = x || 0;
			this.y = y || 0;
		}
	}
	// Retourne l'angle (en radian) formé avec le point
	Point.prototype.angle = function(point) {
		return Math.atan2(point.y - this.y, point.x - this.x);
	};
	// Retourne la distance entre les points
	Point.prototype.range = function(point) {
		return Math.sqrt(Math.pow(point.x - this.x, 2) + 
			Math.pow(point.y - this.y, 2));
	};
	// Translate le point de vx en x, et de vy en y
	// vx peut être un Vector
	Point.prototype.translate = function(vx, vy) {
		if (vx instanceof Vector) {
			this.x += vx.x;
			this.y += vx.y;
		}
		else {
			this.x += vx;
			this.y += vy;		
		}
	};
	// Effectue la rotation autour de l'origine selon l'angle (en radian)
	Point.prototype.pivot = function(origin, angle) {
		var cos = Math.cos(angle);
		var sin = Math.sin(angle);
		var x2 = this.x - origin.x;
		var y2 = this.y - origin.y;
		this.x = cos*x2 - sin*y2 + origin.x;
		this.y = sin*x2 + cos*y2 + origin.y;
	};
	// Retourne vrai si le point est dans le polygonne
	Point.prototype.content = function(polygon) {
		for (var i = 0; i < polygon.length; i++) {
			var i2 = (i + 1)%polygon.length;
			var v1 = new Point(	// Vecteur entre 2 points du polygone
				polygon[i2].x - polygon[i].x,
				polygon[i2].y - polygon[i].y);
			var v2 = new Point(	// Vecteur entre le point (this) et un point du polygone
				this.x - polygon[i].x,
				this.y - polygon[i].y);
			// Calcule si le point est à droite du trait entre les 2 points du vecteur 1
			// Si c'est le cas, le point sera forcément en dehors
			if (v1.x*v2.y - v1.y*v2.x < 0) {
				return false;
			}
		}
		// Sinon si le point est toujours à gauche, il est à l'intérieur
		return true;
	};
	// Dessine un cercle
	Point.prototype.draw = function(ref, width, color, style, lineWidth) {
		ctx.beginPath();
		ctx.arc(this.x - ref.x, this.y - ref.y, width, 0, PI2);
		ctx.lineWidth = lineWidth || 1;
		if (style === 'fill') {
			ctx.fillStyle = color;
			ctx.fill();
		}
		else {
			ctx.strokeStyle = color;
			ctx.stroke();
		}	
	};

/* Point dans un espace 3D */
	function Point3D(x, y, z) {
		Point.call(this, x, y);
		this.z = z;
	}
	// Hérite de Point
	Point3D.prototype = Object.create(Point.prototype, {
		constructor:{value:Point3D,enumerable:false,writable:true,configurable:true}
	});

/* Vecteur */
	function Vector(coeff, direction) {
		this.coeff = coeff || 0;
		this.direction = direction || 0;
		Point.call(this);
		this.refreshXY();
	}
	// Hérite de Point
	Vector.prototype = Object.create(Point.prototype, {
		constructor:{value:Vector,enumerable:false,writable:true,configurable:true}
	});
	// Défini le coefficient du vecteur
	Vector.prototype.setCoeff = function(coeff) {
		this.coeff = coeff;
		this.refreshXY();
	};
	// Défini la direction du vecteur
	Vector.prototype.setDirection = function(direction) {
		this.direction = direction;
		this.refreshXY();
	};
	// Etend le vecteur
	Vector.prototype.extend = function(width) {
		this.coeff += width;
		this.refreshXY();
	};
	// Multiplie le vecteur
	Vector.prototype.scale = function(scale) {
		this.coeff *= scale;
		this.refreshXY();
	};
	// Effectue la rotation du vecteur
	Vector.prototype.rotate = function(angle) {
		this.direction = (this.direction + angle)%PI2;
		this.refreshXY();
	};
	// Effectue la somme de 2 vecteurs
	Vector.prototype.sum = function(vector) {
		this.translate(vector.x, vector.y);
		this.refreshWay();
	};
	// Actualise les coordonnées du vecteur
	Vector.prototype.refreshXY = function() {
		this.x = this.coeff*Math.cos(this.direction);
		this.y = this.coeff*Math.sin(this.direction);
	};
	// Actualise le coefficient et l'angle du vecteur
	Vector.prototype.refreshWay = function() {
		this.coeff = this.range(ZERO);
		this.direction = ZERO.angle(this);
	};

/* Polygone */
	// Si l'origine n'est pas spécifiée, cela devient le point (0,0)
	// Si le tableau est vide, le polygone n'aura tout simplement pas de points
	// (on pourra les ajouter par la suite avec "push")
	function Polygon(tab_x, tab_y, origin) {
		var ref = origin || ZERO;
		for (var i = 0; i < tab_x.length; i++) {
			this.push(new Point(tab_x[i] + ref.x, tab_y[i] + ref.y));
		}
	}
	// Hérite de Array
	Polygon.prototype = Object.create(Array.prototype, {
		constructor:{value:Polygon,enumerable:false,writable:true,configurable:true}
	});
	// Translate le polygone de vx en x, et de vy en y
	// vx peut être un Vector
	Polygon.prototype.translate = function(vx, vy) {
		for (var point of this) {
			point.translate(vx, vy);
		}
	};
	// Effectue la rotation du polygone autour de l'origine selon l'angle (en radian)
	Polygon.prototype.pivot = function(origin, angle) {
		for (var point of this) {
			point.pivot(origin, angle);
		}
	};
	// Retourne vrai si les 2 polygonnes se touche
	// (au moins un des points de l'un est dans l'autre)
	Polygon.prototype.collision = function(polygon) {
		for (var point of this) {
			if (point.content(polygon)) {
				return true;
			}
		}
		return false;
	};
	// Dessine le polygone
	Polygon.prototype.draw = function(ref, color, style, lineWidth) {
		ctx.beginPath();
		ctx.moveTo(this[0].x - ref.x, this[0].y - ref.y);
		for (var point of this) {
			ctx.lineTo(point.x - ref.x, point.y - ref.y);
		}
		ctx.closePath();
		ctx.lineWidth = lineWidth || 1;
		if (style === 'fill') {
			ctx.fillStyle = color;
			ctx.fill();
		}
		else {
			ctx.strokeStyle = color;
			ctx.stroke();
		}
	};

/* Entité mobile */
	function Entity(origin, hitbox, vector, orientation, rotation) {
		this.origin = new Point(origin);
		this.hitbox = hitbox || new Polygon([]);
		this.vector = (vector) ? new Vector(vector.coeff, vector.direction) : new Vector();
		this.orientation = orientation || 0;
		this.rotation = rotation || 0;
		this.speed_max = this.vector.coeff;
		this.friction = 1;
	}
	// Défini la vitesse
	Entity.prototype.setSpeed = function(v) {
		this.vector.setCoeff(v);
	};
	// Défini l'orientation (en radian)
	Entity.prototype.setOrientation = function(orientation) {
		this.hitbox.pivot(this.origin, orientation - this.orientation);
		this.orientation = orientation%PI2;
	};
	// Translate l'entité de vx en x, et de vy en y
	// vx peut être un Vector
	Entity.prototype.translate = function(vx, vy) {
		this.origin.translate(vx, vy);
		this.hitbox.translate(vx, vy);
	};
	// Effectue une rotation de l'entité en modifiant son orientation
	//    selon l'angle (en radian)
	Entity.prototype.rotate = function(angle) {
		this.setOrientation(this.orientation + angle);
	};
	// Actualise l'orientation en fonction de la vitesse de rotation
	Entity.prototype.refreshOrientation = function() {
		this.rotate(this.rotation);
	};
	// Actualise la position en fonction de la vitesse
	Entity.prototype.refreshPosition = function() {
		this.translate(this.vector);
	};
	// Actualise la vitesse en prenant en compte la friction
	Entity.prototype.refreshSpeed = function() {
		this.vector.scale(this.friction);

		if (this.vector.coeff > this.speed_max) {
			this.vector.setCoeff(this.speed_max);
		}
		else if (this.vector.coeff < .1) {
			this.vector.setCoeff(0);
		}
	};

/* Animation */
	function Animation(name, time_over, origin, color, orientation) {
		Entity.call(this, origin, null, null, orientation);
		this.name = name;
		this.time = 0;
		this.time_over = time_over;
		this.color = color;
	}
	Animation.prototype = Object.create(Entity.prototype, {
		constructor:{value:Animation,enumerable:false,writable:true,configurable:true}
	});
	// Retourne vrai si l'animation est terminée
	Animation.prototype.isOver = function() {
		return this.time > this.time_over;
	};
	// Ecoule le temps de l'animation
	Animation.prototype.timeFlows = function() {
		this.time++;
	};
	// Dessine l'animation
	Animation.prototype.draw = function(ref) {
		var opacity = 1 - this.time/(this.time_over+1);
		switch (this.name) {
			// Particule qui disparait vite en rapetissant
			case 'trace' : {
				this.origin.draw(ref, this.time_over - this.time, 
					getFillColor(this.color-1, 100), 'fill'); break;
			}
			// Triangles qui s'éloignent de l'origine
			case 'eclat' : {
				var triangle = new Polygon([0,  1 - this.time_over, this.time_over - 1],
					[0,  6 + this.time_over, 6 + this.time_over], this.origin);
				triangle.translate(0, 4*this.time);
				triangle.pivot(this.origin, this.orientation);
				for (var i = 0; i < 5; i++) {
					triangle.pivot(this.origin, PI2_5);
					var gray = 50 + Math.floor(toDeg(Math.abs(triangle[0].angle(triangle[1])))/2);
					triangle.draw(ref, getFillColor(this.color-1, gray), 'fill');
					triangle.draw(ref, 'black', 'stroke', 1);
				} break;
			}
			// Cercle grossissant dont les bords s'affinent jusqu'à disparaîtres
			case 'blast' : {
				this.origin.draw(ref, this.time*5, getFillColor(this.color, 10*random(5, 17)),
					'stroke', (this.time_over - this.time)*2); break;
			}
			// Rotation de disques autour de l'origine
			case 'bonus_on' : {
				var color = getFillColor(0, 255, opacity);
				var new_ref = new Point(-this.origin.x + ref.x, -this.origin.y + ref.y);
				for (var j = 0; j < 2; j++) {
					var point = new Point(15+this.time*4, 0);
					point.pivot(ZERO, PI_7_5*this.time + PI*j);
					for (var i = 0; i < 9; i++) {
						point.pivot(ZERO, PI_10);
						point.draw(new_ref, i, getFillColor(this.color, i*20, opacity), 'fill');
						point.draw(new_ref, i, color, 'stroke', 1);
					}
				} break;
			}
			// Cercle grossissant et disparaissant autour de l'origine
			case 'bonus_off' : {
				for (var i = 1; i < 4; i++) {
					this.origin.draw(ref, Math.pow(2, i)*this.time/1.5 + 10, 
						getFillColor(this.color, i*(50+this.time*2), opacity),
						'stroke', 6 - i*2*(1 - opacity));
				} break;
			}
			// Particules d'explosion d'objets
			case 'particle' : {
				var point = new Point(this.origin);
				point.translate(new Vector(this.time*this.time_over/5, this.orientation + PI));
				point.draw(ref, random(1, 3), getFillColor(this.color, 175, opacity), 'fill');
			}
		}
	};

/* Tableau d'animation */
	function AnimationTab() {}
	// Hérite de Array
	AnimationTab.prototype = Object.create(Array.prototype, {
		constructor:{value:AnimationTab,enumerable:false,writable:true,configurable:true}
	});
	// Fait bouger toutes les animations
	AnimationTab.prototype.translate = function(vx, vy) {
		for (var animation of this) {
			animation.translate(vx, vy);
		}
	};
	// Actualise toutes les animations (temps qui s'écoule et suppression à la fin)
	AnimationTab.prototype.refresh = function() {
		for (var i = 0; i < this.length; i++) {
			this[i].timeFlows();
			if (this[i].isOver()) {
				this.splice(i, 1);
				i--;
			}
		}
	};
	// Dessine toutes les animations
	AnimationTab.prototype.draw = function(ref) {
		for (var animation of this) {
			animation.draw(ref);
		}
	};

/* Vaisseau */
	function Ship(origin) {
		Entity.call(this, origin, new Polygon(
			[ 16, -13, -15, -15, -13,  16],
			[ -1, -15, -15,  16,  16,   2], origin));
		this.tab_animation = new AnimationTab();
		this.speed_max = SPEED_MAX;
		this.friction = FRICTION_COEFF;
	}
	// Hérite de Entity
	Ship.prototype = Object.create(Entity.prototype, {
		constructor:{value:Ship,enumerable:false,writable:true,configurable:true}
	});
	// Actualise la position du vaisseau et des animations rattachées
	Ship.prototype.refreshPosition = function() {
		this.translate(this.vector);
		this.tab_animation.translate(this.vector);
	};
	// Retourne un vecteur aligné à l'angle correspondant
	function generateInputVector(k1, k2, angle) {
		var range = (k2 - k1)*ACCELERATION_COEFF;
		var direction = (range > 0) ? angle : angle + PI;
		return new Vector(Math.abs(range), direction);
	}
	// Actualise le vecteur vitesse en fonction des touches pressées
	Ship.prototype.inputVector = function(keyTime) {
		var vector	 = generateInputVector(keyTime.left, keyTime.right, 0)
		var vector_2 = generateInputVector(keyTime.up, keyTime.down, PI_2);
		this.vector.sum(vector);
		this.vector.sum(vector_2);
		this.refreshSpeed();
		this.refreshPosition();
	};
	// Actualise l'oriention
	Ship.prototype.inputOrientation = function(interaction) {
		if (interaction.cursor_activate) {
			this.setOrientation(this.origin.angle(interaction.cursor));
		}
		else {
			this.rotate(ROTATION_SENSIBIITY*
				(interaction.keyTime.rotate_right
				- interaction.keyTime.rotate_left));
		}
	};
	// Dessine le vaisseau
	Ship.prototype.draw = function(ref, color, bonus) {
		ctx.save();
		ctx.translate(this.origin.x - ref.x, this.origin.y - ref.y);
		ctx.rotate(this.orientation);
		// Si il y a bonus activé
		if (bonus.isActivated()) {
		// Vaisseau
			// Bonus Triple
			if (bonus.id == 4) {
				for (var i = 0; i < 3; i++) {
					ctx.drawImage(tileset_ship, 
						34*bonus.id, 33*color, 33, 33, -10, -16, 33, 33);
					ctx.rotate(PI2_3);
				}
			}
			// Normal
			else {
				ctx.drawImage(tileset_ship, 
					34*bonus.id, 33*color, 33, 33, -16, -16, 33, 33);
			}
		// Jauge de temps du bonus
			var angle = 90/BONUS_TIME_OUT/CPS*bonus.time;
			ctx.strokeStyle = getFillColor(color, 50);
			ctx.lineWidth = 3;
			ctx.beginPath();
			// Recentre le cercle si c'est le bonus Triple (pour des questions d'esthétiques)
			if (bonus.id == 4) {
				ctx.arc(0, 0, 25, angle, -angle);
			}
			// Sinon le décale vers le bas (toujours pour des questions d'esthétiques)
			else {
				ctx.arc(-5, 0, 25, angle, -angle);
			}
			ctx.stroke();
		}
		// Sinon le vaisseau normal
		else {
			ctx.drawImage(tileset_ship, 0, 33*color, 33, 33, -16, -16, 33, 33);
		}
		ctx.restore();
	};

/* Missile */
	function Bullet(id, origin, vector) {
		Entity.call(this, origin, new Polygon([origin.x],[origin.y]), vector, vector.direction);
		this.id = id;
		this.shoot_origin = new Point(origin);
	}
	// Hérite de Entity
	Bullet.prototype = Object.create(Entity.prototype, {
		constructor:{value:Bullet,enumerable:false,writable:true,configurable:true}
	});
	// Dessine la hitbox qui ne se limite qu'à un point
	Bullet.prototype.drawHitbox = function(ref, color, style, lineWidth) {
		var polygon = new Polygon([-2,  2, 2, -2],[-2, -2, 2,  2], this.origin);
		polygon.draw(ref, color, style, lineWidth);
	};
	// Dessine le missile
	Bullet.prototype.draw = function(ref, color) {
		ctx.save();
		// Si c'est le missile Onde, on translate le contexte par l'origine du tir
		if (this.id == 3) {
			ctx.translate(this.shoot_origin.x - ref.x, this.shoot_origin.y - ref.y);
		}
		else {
			ctx.translate(this.origin.x - ref.x, this.origin.y - ref.y);
		}
		ctx.rotate(this.orientation);
		switch (this.id) {
			// Laser, juste un tir plus long
			case 2 : {
				ctx.drawImage(tileset_bullet, 0, 3*color, 15, 3, -30, -1, 45, 3); break;
			}
			// Onde, une forme d'arc de cercle dégradé en fonction de l'origine du tir
			case 3 : {
				var angle = PI_36/(this.shoot_origin.range(this.origin)/100);
				ctx.lineWidth = 4;
				for (var i = 0; i < 3; i++) {
					ctx.strokeStyle = getFillColor(color, 25+(3-i)*50);
					ctx.beginPath();
					ctx.arc(0, 0, this.shoot_origin.range(this.origin)-i*3, 
						-angle, angle);
					ctx.stroke();
				} break;
			}
			// Mini, plus fin
			case 4 : {
				ctx.drawImage(tileset_bullet, 0, 3*color, 15, 2, -7.5, -1, 15, 2); break;
			}
			case 5 : {
				ctx.drawImage(tileset_bullet, 0, 3*color, 15, 3, -5, -1, 10, 3); break;
			}
			case 6 : {
				for (var i = -2; i < 3; i++) {
					ctx.drawImage(tileset_bullet, 0, 3*color, 15, 3, -6, -1+i*2, 12, 3);
				} break;
			}
			// Normal
			default : {
				ctx.drawImage(tileset_bullet, 0, 3*color, 15, 3, -7.5, -1, 15, 3);
			}
		}
		ctx.restore();
//		ctx.fillStyle = getFillColor(color, 200);
//		var coordonnees = Math.round(this.origin.x) + ' : ' + Math.round(this.origin.y);
//		ctx.fillText(coordonnees, 
//			this.origin.x + 1 - ctx.measureText(coordonnees).width/2-ref.x,
//			this.origin.y + 28 - ref.y);
	};

/* Astéroïd */
	function Asteroid(size, origin, speed_vector, rotation, bonus_id) {
		Entity.call(this, origin, null, speed_vector, null, rotation);
		this.size = size;
		this.bonus_id = bonus_id;

		// Génération d'un polygone irrégulier bien proportionné
		var nb_side = random(NB_SIDE_MIN, NB_SIDE_MAX);
		var nb_side_generated = 0;
		var min_deg = 360/nb_side;
		for (var deg = 0; deg < 360 && nb_side_generated < nb_side; deg++)
		{
			if (deg >= min_deg*nb_side_generated || 
				(!random(0, 10 + NB_SIDE_MAX*10 - nb_side*10) && 
					deg >= min_deg*(nb_side_generated - 1) + 30))
			{
				var vector = new Vector(10 + this.size*10, toRad(deg));
				this.hitbox.push(new Point(this.origin.x + vector.x, this.origin.y + vector.y));
				deg = min_deg*nb_side_generated;
				nb_side_generated++;
			}
		}
	}
	// Hérite de Entity
	Asteroid.prototype = Object.create(Entity.prototype, {
		constructor:{value:Asteroid,enumerable:false,writable:true,configurable:true}
	});
	// Retounre la valeur de l'astéroïde
	Asteroid.prototype.getScore = function() {
		return ASTEROID_MAX_SIZE + 1 - this.size;
	};
	// Retourne un tableau de 2 astéroïdes
	// correspondants aux 2 morceaux de l'astéroïde explosé
	Asteroid.prototype.getTabSplit = function(orientation) {
		var tab = [];
		for (var i = -1; i < 2; i += 2) {
			var direction = orientation + i*(PI_16 + PI_128*random(0, 8));
			var vector = new Vector(2*this.getScore(), direction);
			tab.push(new Asteroid(this.size-1,
				new Point(this.origin), vector, this.rotation, -1));
		}
		return tab;
	};
	// Retourne vrai si l'astéroïdes contient un bonus
	Asteroid.prototype.isBonus = function() {
		return this.bonus_id > 0;
	};
	// Déplace et effectue la rotation de l'astéroïde
	Asteroid.prototype.refreshMove = function() {
		this.refreshSpeed();
		this.refreshPosition();
		this.refreshOrientation();
	};
	// Dessine l'astéroïde
	Asteroid.prototype.draw = function(ref, opacity) {
		// Faces du polygone
		for (var i = 0; i < this.hitbox.length; i++)
		{
			// Création du triangle
			var i2 = (i+1)%this.hitbox.length;
			var polygon = new Polygon(
				[this.origin.x, this.hitbox[i].x, this.hitbox[i2].x],
				[this.origin.y, this.hitbox[i].y, this.hitbox[i2].y]);
			// Calcul de la nuance, avec l'orientation du triangle et une sombre formule
			var gray = 10 + Math.floor(toDeg(
				Math.abs(this.origin.angle(this.hitbox[i])) +
				Math.abs(this.origin.angle(this.hitbox[i2])))/3);
			gray = Math.round(gray*opacity);
			polygon.draw(ref, getFillColor(this.bonus_id-1, gray), 'fill');
		}
		// Ligne de contour noire
		this.hitbox.draw(ref, 'black', 'stroke', 1);
	};

/* Bonus */
	function Bonus(id, origin) {
		Entity.call(this, origin, null, new Vector(5, 0));
		this.id = id;
		this.time = -1;
	}
	Bonus.prototype = Object.create(Entity.prototype, {
		constructor:{value:Bonus,enumerable:false,writable:true,configurable:true}
	});
	// Retourne vrai si le bonus est défini
	Bonus.prototype.isDefined = function() {
		return this.id > 0;
	};
	// Retourne si le bonus est activé (lorsque il touche le joueur qui l'a libéré)
	Bonus.prototype.isActivated = function() {
		return this.time != -1;
	};
	// Retourne vrai si le temps du bonus est écoulé
	Bonus.prototype.isOver = function() {
		return this.time >= BONUS_TIME_OUT;
	};
	// Fait accélérer le bonus
	Bonus.prototype.accelerate = function() {
		this.vector.scale(BONUS_ACCELERATION);
	};
	// Active le bonus
	Bonus.prototype.activate = function() {
		this.time = 0;
	};
	// Ecouler le temps du bonus
	Bonus.prototype.timeFlows = function() {
		this.time++;
	};
	// Dessine la hitbox
	Bonus.prototype.drawHitbox = function(ref, color, style, lineWidth) {
		var polygon = new Polygon([-4,  4, 4, -4], [-4, -4, 4,  4], this.origin);
		polygon.draw(ref, color, style, lineWidth);
	};
	// Dessine le bonus
	Bonus.prototype.draw = function(ref) {
		this.origin.draw(ref, 5, getFillColor(this.id-1, 150), 'fill');
	};

/* Intéractions */
// Booleens des actions et curseur du joueur
	function Interaction() {
		this.keyCode = {
			up: 90, down: 83, left: 81, right: 68,
			rotate_left: 37, rotate_right: 39, 
			shoot: 38, switch_auto_shoot: 32};
		this.cursor = new Point();
		this.cursor_activate = true;
		this.keyTime = {
			up: 0, down: 0, left: 0, right: 0,
			rotate_left: 0, rotate_right: 0, 
			shoot: 0, switch_auto_shoot: 0};
		this.clic_shoot = false;
		this.clic_next = false;
		this.time_shoot = 0;
		this.min_time_shoot = MIN_TIME_SHOOT;
		this.max_time_shoot = MAX_TIME_SHOOT;
		this.auto_shoot = 0;
	}
	// Actualise les coordonnées du curseur
	Interaction.prototype.setCursorPosition = function(event, ref) {
		var current_cursor = new Point(
			event.clientX - canvas.getBoundingClientRect().left + ref.x,
			event.clientY - canvas.getBoundingClientRect().top + ref.y);
		if (current_cursor.x != this.cursor.x || current_cursor.y != this.cursor.y) {
			this.cursor_activate = true;
			this.cursor = current_cursor;
		}
	};
	// Défini l'activation du curseur
	Interaction.prototype.setCursorActivation = function(state) {
		this.cursor_activate = state;
	};
	// Defini les temps min et max avant le tir
	Interaction.prototype.setTimeLock = function(bonus_id) {
		this.min_time_shoot = MIN_TIME_SHOOT*TAB_BONUS_COEFF[bonus_id];
		this.max_time_shoot = MAX_TIME_SHOOT*TAB_BONUS_COEFF[bonus_id];
	};
	// Défini l'état d'une action, 
	// en initialisant à 1 le temps d'appuis de sa touche associée
	Interaction.prototype.setKeyState = function(key, state) {
		this.keyTime[key] = (state == true) ? 1 : 0;
	};
	// Défini le clic ponctuel de la touche de tir
	Interaction.prototype.setClicShoot = function(state) {
		this.clic_shoot = state;
	};
	// Revoit vrai si le missile doit être généré
	// (lorsque le délai est a 1 tic)
	Interaction.prototype.isShoot = function() {
		return this.time_shoot == 1;
	};
	// Actualise l'état de l'activation du curseur
	Interaction.prototype.refreshCursorActivation = function() {
		if (this.keyTime.rotate_left > 0 
			|| this.keyTime.rotate_right > 0) {
			this.cursor_activate = false;
		}
	};
	// Actualise le temps d'une action
	Interaction.prototype.refreshKeyTime = function() {
		// On parcoure le tableau du temps des actions
		for (var i in this.keyTime) {
			// On incrémente le temps si l'action est en cours
			if (this.keyTime[i] > 0 && this.keyTime[i] < 10) {
				if (i == 'shoot' || i == 'rotate_left' || i == 'rotate_right') {
					this.keyTime[i] == 1;
				}
				else {
					this.keyTime[i]++;
				}
			}
		}
	};
	// Actualise l'état du tir auto
	Interaction.prototype.refreshAutoShoot = function() {
		if (this.keyTime.switch_auto_shoot == 2) {
			this.auto_shoot = (this.auto_shoot+1)%2;
		}
	};
	// Actualise le tir
	Interaction.prototype.refreshShoot = function() {
		// Si on vient de cliquer sur une touche de tir
		if (this.clic_shoot) {
			this.clic_shoot = false;	// On désactive le clic ponctuel
			if (!this.clic_next) {		// Si l'on avait pas encore enregistré de tir
				this.clic_next = true;	// On l'enregistre
			}							// Sinon on amène directement le délai de tir au maximum
			else {						// Ce qui aura pour effet de le réinitialiser et de le
				this.time_shoot = this.max_time_shoot+1;	// relancer pour réduire le délai
			}							// de la cadence automatique
		}
		// Si un tir est en cours, le délai s'actualise
		if (this.keyTime.shoot || this.time_shoot > 0 
			|| this.clic_next || this.auto_shoot) {
			this.time_shoot++;
		}
		// Lorsque le délai arrive au minimum et qu'un tir a été enregistré
		// (en gros que le joueur a cliqué vite plusieurs fois d'affilé)*
		// Ou alors que le délai arrive au maximum
		if ((this.time_shoot > this.min_time_shoot && this.clic_next)
			|| (this.time_shoot > this.max_time_shoot)) {
			// On réinitialise le délai et on désactive le tir en cours
			this.time_shoot = 0;
			this.clic_next = false;
		}
	};
	// Actualise l'état des actions
	Interaction.prototype.refreshKeyState = function(key, state) {
		// On parcoure le tableau des codes de touches
		for (var i in this.keyCode) {
			// lorsqu'on a trouvé une touche correspondante
			if (key == this.keyCode[i]) {
				// On défini son état
				if ((this.keyTime[i] == 0 && state == true) 
					|| (this.keyTime[i] > 0 && state == false)) {
					this.setKeyState(i, state);
				} break;
			}
		}
		if (key == this.keyCode.shoot) {
			this.cursor_activate = false;
			this.clic_shoot = true;
		}
	};

/* Statistiques de tir */
	function ShootStat() {
		this.nb_activated = 0;
		this.nb_shoot = 0;
		this.object = [];	// Objets touché : vaisseau, 1, 2, 3, 4
		for (var i = 0; i < ASTEROID_MAX_SIZE + 1; i++) {
			this.object.push(0);
		}
	}

/* Joueur */
	function Player(name, color, origin) {
		this.name = name;
		this.color = color;
		this.ship = new Ship(origin);
		this.tab_bullet = [];
		this.bonus = new Bonus(0);

		this.interaction = new Interaction();
		
		this.time_blink = 0;
		this.score_malus;

		this.stat = {
			rank : 0,
			score : 0,
			score_max : 0,
			score_total : 0,
			nb_player : 0,
			nb_asteroid  : 0,
			precision : 100,
			collision : 0,
			bonus : []
		}
		for (var i = 0; i < BONUS_NB + 1; i++) {
			this.stat.bonus.push(new ShootStat());
		}

		this.tab_animation = new AnimationTab();
		this.animation_on = 1;
	}
	// Actualise la position et l'orientation du vaisseau
	Player.prototype.refreshShip = function() {
		this.ship.inputVector(this.interaction.keyTime);
		this.ship.inputOrientation(this.interaction);
	};
	// Actualise la position du curseur dans le référentiel
	Player.prototype.refreshCursor = function(ref, old_ref) {
		this.interaction.cursor.translate(ref.x - old_ref.x, ref.y - old_ref.y);
	};
	// Génère des missiles
	Player.prototype.generateShoot = function() {
		if (this.interaction.isShoot() && !this.isBlinking()) {
			var speed_vector = new Vector(BULLET_SPEED, this.ship.orientation);
			var origin = new Point(this.ship.origin);
			switch (this.bonus.id*this.bonus.isActivated()) {
				// Double
				case 1 : {
					var vector = new Vector(-5, speed_vector.direction + PI_2);
					for (var i = -1; i < 2; i += 2) {
						origin.translate(vector);
						this.tab_bullet.push(new Bullet(1, origin, speed_vector));
						vector.setCoeff(10);
					} break;
				}
				// Laser
				case 2 : {
					origin.translate(new Vector(-1, speed_vector.direction + PI));
					for (var i = 0; i < 5; i++) {
						speed_vector.extend(i*2);
						this.tab_bullet.push(new Bullet(2, origin, speed_vector));
					} break;
				}
				// Onde
				case 3 : {
					speed_vector.rotate(-2*PI_36);
					origin.translate(new Vector(-1, speed_vector.direction));
					for (var i = -2; i < 3; i++) {
//for (var i = 0; i < 72; i++) {
						this.tab_bullet.push(new Bullet(3, origin, speed_vector));
						speed_vector.rotate(PI_36);
					} break;
				}
				// Triple
				case 4 : {
					for (var i = 0; i < 3; i++) {
						this.tab_bullet.push(new Bullet(4, origin, speed_vector));
						speed_vector.rotate(PI2_3);
					} break;
				}
				// Normal, Tête chercheuse, Fragmentation
				default : {
					this.tab_bullet.push(new Bullet(this.bonus.id, origin, speed_vector));
				}
			}
			this.stat.bonus[this.bonus.id].nb_shoot++;
		}
	};
	Player.prototype.refreshBullet = function(map_hitbox) {
		for (var i = 0; i < this.tab_bullet.length; i++) {
			this.tab_bullet[i].refreshSpeed();
			this.tab_bullet[i].refreshPosition();
			// Destruction si sort de la map
			if (!this.tab_bullet[i].origin.content(map_hitbox)) {
				this.refreshPrecision();
				this.tab_bullet.splice(i, 1);
				i--;
			}
		}
	};
	// Retourne vrai si le bonus est proche du vaisseau
	Player.prototype.bonusNear = function() {
		return this.bonus.origin.range(this.ship.origin) < 6;
	};
	Player.prototype.setBonus = function(asteroid) {
		this.bonus = new Bonus(asteroid.bonus_id, asteroid.origin);
	};
	Player.prototype.refreshBonus = function() {
		// Si le joueur à touché un astéroïde bonus
		if (this.bonus.isDefined()) {
			// Déplace le bonus jusqu'au joueur et l'active
			if (!this.bonus.isActivated()) {
				if (!this.bonusNear()) {
					this.bonus.accelerate();
					var vector = new Vector(1, this.bonus.origin.angle(this.ship.origin));
					for (var i = 0; i < this.bonus.vector.coeff && 
							!this.bonus.isActivated(); i++) {
						this.bonus.translate(vector);
						// Active le bonus
						if (this.bonusNear()) {
							break;
						}
						else if (i%10 == 0) {
							this.tab_animation.push(new Animation('trace', 4,
								this.bonus.origin, this.bonus.id));
						}
					}
				}
				if (this.bonusNear()) {
					this.bonus.activate();
					this.interaction.setTimeLock(this.bonus.id);
					this.ship.tab_animation.push(new Animation(
						'bonus_on', 20, this.ship.origin, this.bonus.id-1));
					this.stat.bonus[this.bonus.id].nb_activated++;
				}
			}
			// Si il est activé on écoule son temps
			else if (!this.bonus.isOver()) {
				this.bonus.timeFlows();
			}
			// Dès que le temps est écoulé, on le désactive
			else {
				this.bonus = new Bonus(0);
				this.interaction.setTimeLock(0);
				this.ship.tab_animation.push(new Animation('bonus_off', 20,  
					this.ship.origin, this.color));
			}
		}
	};
	// Collision avec un astéroïde
	Player.prototype.asteroidCollision = function(asteroid) {
		if (this.ship.hitbox.collision(asteroid.hitbox) && 
					!this.isBlinking()) {
			// Bonus
			if (this.bonus.isActivated()) {
				this.bonus = new Bonus(0);
				this.interaction.setTimeLock(0);
			}
			this.activeBlink();
			// Destruction de l'astéroïde
			if (this.animation_on) {
				this.tab_animation.push(new Animation('eclat', 6, asteroid.origin, 
					asteroid.bonus_id, toRad(random(0, 7)*10)));
				for (var j = 0, c = random(3, 4); j < c; j++) {
					var point = new Point(this.ship.origin);
					point.translate(8*random(-3, 3), 8*random(-3, 3));
					this.ship.tab_animation.push(new Animation('blast', j*3 + 2, 
						point, this.color));
				}
			}
			this.stat.collision++;
			return 1;
		}
		return 0;
	};
//	Player.prototype.shipCollision = function(ship) {
//		return this.ship.hitbox.collision(ship.hitbox);
//	};
	Player.prototype.animSplit = function(bullet, asteroid) {
		this.tab_animation.push(new Animation('eclat', 4 + asteroid.size, 
			asteroid.origin, asteroid.bonus_id, toRad(random(0, 7)*10)));
		this.tab_animation.push(new Animation('blast', random(3, 4),  
			bullet.origin, this.color));
	};
	Player.prototype.refreshAnimation = function() {
		this.tab_animation.refresh();
		this.ship.tab_animation.refresh();
	};
	// Retourne vrai si le vaisseau est en train de clignoter (après une collision)
	Player.prototype.isBlinking = function() {
		return this.time_blink > 0;
	};
	// Active le clignotement du vaisseau
	Player.prototype.activeBlink = function() {
		this.time_blink = 1;
		this.score_malus = this.stat.score/TIME_BLINK;
	};
	// Actualise le clognotement du vaisseau
	Player.prototype.timeFlows = function() {
		if (this.time_blink <= TIME_BLINK/2) {
			this.stat.score = (this.stat.score - this.score_malus) > 0 ? 
				(this.stat.score - this.score_malus) : 0;
		}
		else if (this.stat.score != Math.floor(this.stat.score)) {
			this.stat.score = Math.floor(this.stat.score);
		}
		this.time_blink = (this.time_blink+1)%TIME_BLINK;
	};
	Player.prototype.incrementShootStat = function(id, id_object) {
		this.stat.bonus[id].object[id_object]++;
		if (id_object > 0) {
			this.stat.nb_asteroid++;
		}
		else {
			this.stat.nb_player++;
		}
		this.refreshPrecision();
	};
	Player.prototype.refreshPrecision = function() {
		const tab = [1, 2, 5, 5, 3, 1];
		var nb_bullet = 0;
		for (var k = 0; k < this.stat.bonus.length; k++) {
			nb_bullet += this.stat.bonus[k].nb_shoot*tab[k];
		}
		this.stat.precision = Math.round(100*(this.stat.nb_asteroid + this.stat.nb_player)/nb_bullet);
	};
	Player.prototype.refreshScore = function(score) {
		this.stat.score = (this.stat.score + score > 0) ? this.stat.score + score : 0;
		if (score > 0) {
			this.stat.score_total += score;
		}
		if (this.stat.score_max < this.stat.score) {
			this.stat.score_max = this.stat.score;
		}
	};
	// Dessine les information relatives au joueur
	Player.prototype.draw = function(ref) {
		ctx.fillStyle = getFillColor(this.color, 200);
		ctx.fillText(this.name, 
			this.ship.origin.x + 1 - ctx.measureText(this.name).width/2 - ref.x,
			this.ship.origin.y + 28 - ref.y);
		var score = new Intl.NumberFormat().format(Math.floor(this.stat.score));
		ctx.fillText(score, 
			this.ship.origin.x + 1 - ctx.measureText(score).width/2 - ref.x,
			this.ship.origin.y + 42 - ref.y);

//		var coordonnees = Math.round(this.ship.origin.x) + ' : ' + Math.round(this.ship.origin.y);
//		ctx.fillText(coordonnees, 
//			this.ship.origin.x + 1 - ctx.measureText(coordonnees).width/2 - ref.x,
//			this.ship.origin.y + 56 - ref.y);
//		var coordonnees = Math.round(ref.x) + ' : ' + Math.round(ref.y);
//		ctx.fillText(coordonnees, 
//			this.ship.origin.x + 1 - ctx.measureText(coordonnees).width/2 - ref.x,
//			this.ship.origin.y + 70 - ref.y);
//		var coordonnees = Math.round(ref.x + canvas.width/2) + ' : ' + Math.round(ref.y + canvas.height/2);
//		ctx.fillText(coordonnees, 
//			this.ship.origin.x + 1 - ctx.measureText(coordonnees).width/2 - ref.x,
//			this.ship.origin.y + 84 - ref.y);
	};

/* Trou Noir */
	function BlackHole(origin) {
		this.origin = new Point(origin);
		do {
			this.target = new Point(
				random(BLACK_HOLE_MAX_SIZE*2, MAP_SIZE - BLACK_HOLE_MAX_SIZE*2),
				random(BLACK_HOLE_MAX_SIZE*2, MAP_SIZE - BLACK_HOLE_MAX_SIZE*2));
		} while (this.targetNear());
		var vector = new Vector(1, this.origin.angle(this.target));
		Entity.call(this, this.origin, null, vector);
		this.size = 1;
		this.over = false;
		this.reachTarget = false;
		this.tab_animation = new AnimationTab();
	}
	BlackHole.prototype = Object.create(Entity.prototype, {
		constructor:{value:BlackHole,enumerable:false,writable:true,configurable:true}
	});
	BlackHole.prototype.targetNear = function() {
		return this.origin.range(this.target) < BLACK_HOLE_MAX_SIZE;
	};
	BlackHole.prototype.refreshSize = function() {
		if (!this.reachTarget) {
			if (this.size < BLACK_HOLE_MAX_SIZE) {
				this.size += (BLACK_HOLE_MAX_SIZE + 1 - this.size)/20;
				if (this.size > BLACK_HOLE_MAX_SIZE) {
					this.size = BLACK_HOLE_MAX_SIZE;
				}
			}
		}
		else {
			if (this.size > 1) {
				this.size -= (BLACK_HOLE_MAX_SIZE + 1 - this.size)/20;
				if (this.size < 1) {
					this.size = 1;
				}
			}
			else {
				this.size = 1;
				this.over = true;
			}		
		}
	};
	BlackHole.prototype.refresh = function() {
		this.refreshPosition();
		this.tab_animation.translate(this.vector);
		this.refreshSize();
		if (this.targetNear()) {
			this.reachTarget = true;
		}
	};
	BlackHole.prototype.clear = function() {
		this.reachTarget = true;
	};
	BlackHole.prototype.isOver = function() {
		return this.over;
	};
	BlackHole.prototype.draw = function(ref) {
	//	this.origin.draw(ref, this.size, 'black', 'fill');
		this.origin.draw(ref, this.size, 'rgb(75, 75, 75)', 'stroke', 1);
	};

/* Map */
	function Map(size) {
		this.size = size;
		this.hitbox = new Polygon([0, this.size, this.size, 0], [0, 0, this.size, this.size]);
		this.ref = new Point();
		this.center = new Point(this.size/2, this.size/2);
		this.tab_star = [];
		// Génère les étoiles
		for (i = 0; i < NB_ZONE; i++) {
			var x = i*ZONE_SIZE;
			for (j = 0; j < NB_ZONE; j++) {
				var y = j*ZONE_SIZE;
				for (k = 0; k < STAR_BY_ZONE; k++) {
					this.tab_star.push(new Point3D(
						random(x, x + ZONE_SIZE), random(y, y + ZONE_SIZE), random(0, 150)/100));
				}
			}
		}
		// Génère les tours noirs
		this.tab_black_hole = [];
		for (var i = 0; i < NB_BLACK_HOLE; i++) {
			this.generateBlackHole();
		}
	}
	// Défini la position centrale de la map par rapport à l'écran
	Map.prototype.setReferentiel = function(ref) {
		this.ref.x = ref.x - canvas.width/2;
		this.ref.y = ref.y - canvas.height/2;
	};
	Map.prototype.generateBlackHole = function() {
		do {
			var origin = new Point(
				random(BLACK_HOLE_MAX_SIZE*2, MAP_SIZE - BLACK_HOLE_MAX_SIZE*2),
				random(BLACK_HOLE_MAX_SIZE*2, MAP_SIZE - BLACK_HOLE_MAX_SIZE*2));
			var check = false;
			for (var black_hole of this.tab_black_hole) {
				if (origin.range(black_hole.origin) < black_hole.size*4) {
					check = true;
				}
			}
		}
		while (check);
		this.tab_black_hole.push(new BlackHole(origin));
	};
	// Dessine la map
	Map.prototype.draw = function(ref, animation_on) {
		// Lignes des zones
//		ctx.strokeStyle = 'rgb(100, 100, 100,)';
//		ctx.strokeStyle = 'rgba(255, 255, 255, .25)';
//		ctx.lineWidth = 1;
//		ctx.beginPath();
//		for (var i = ZONE_SIZE - this.ref.x; i < this.size - this.ref.x;
//				i += ZONE_SIZE) {
//			ctx.moveTo(i, 0 - ref.y);
//			ctx.lineTo(i, this.size - ref.y);
//		}
//		for (var j = ZONE_SIZE - this.ref.y; j < this.size - this.ref.y;
//				j += ZONE_SIZE) {
//			ctx.moveTo(0 - ref.x, j);
//			ctx.lineTo(this.size - ref.x, j);
//		}
//		ctx.stroke();
	
		// Bords
		this.hitbox.draw(ref, 'white', 'stroke', 1);

		// Etoiles
		for (var star of this.tab_star) {
			var width = (!random(0, 2500) && animation_on) ? star.z*3 : star.z;
			var star_ref = new Point(star.x - ref.x*(star.z/3), star.y - ref.y*(star.z/3));
			var check = false;
			for (var i = 0; i < this.tab_black_hole.length; i++) {
				var black_hole_ref = new Point(this.tab_black_hole[i].origin.x - ref.x, 
					this.tab_black_hole[i].origin.y - ref.y);
				var range = black_hole_ref.range(star_ref);
				if (range < this.tab_black_hole[i].size*2) {
					var direction = black_hole_ref.angle(star_ref);
					var vector = new Vector(
						range*this.tab_black_hole[i].size/(this.tab_black_hole[i].size*2) + 
						this.tab_black_hole[i].size, direction);
					var new_star = new Point(black_hole_ref);
					new_star.translate(vector);
					ctx.fillStyle = (animation_on) ? 
						'rgba(255, 255, 255, ' + random(3, 5)/5 + ')' : 'white';
					ctx.strokeStyle = ctx.fillStyle;
					ctx.lineWidth = vector.coeff/(this.tab_black_hole[i].size*2)*width;
					var star_angle = PI_16 - PI_16*(vector.coeff/(this.tab_black_hole[i].size*2));
					ctx.beginPath();
					ctx.arc(black_hole_ref.x, black_hole_ref.y, vector.coeff, 
						-star_angle + direction, star_angle + direction);
					ctx.stroke();
					check = true;
				}
			}
			if (check == false) {
				var point = new Point(star.x - ref.x*(star.z/3), star.y - ref.y*(star.z/3));
				var color = (animation_on) ? 'rgba(255, 255, 255, ' + random(3, 5)/5 + ')' : 'white';
				point.draw(ZERO, width, color, 'fill');
			}
		}
		for (i = 0; i < this.tab_black_hole.length; i++) {
//			this.tab_black_hole[i].draw(ref);
		}
	};

/* Jeu */
	// Défini la fenêtre de jeu
	function setCanvas() {
		var width = body.getBoundingClientRect().width;
		var height = body.getBoundingClientRect().height;
		canvas.height = height;
		canvas.width = width;
		LB_X = canvas.width - STAT_WIDTH - 10;
		POLYGON_LB = new Polygon([]);
		POLYGON_LB.push(new Point(LB_X, 10));
		POLYGON_LB.push(new Point(LB_X + STAT_WIDTH, 10));
		POLYGON_LB.push(new Point(LB_X + STAT_WIDTH, 16 + (STAT_TEXT + 6)*NB_LB));
		POLYGON_LB.push(new Point(LB_X, 16 + (STAT_TEXT + 6)*NB_LB));
		POLYGON_T = new Polygon([]);
		POLYGON_T.push(new Point(20 + STAT_WIDTH, 10));
		POLYGON_T.push(new Point(LB_X - 10, 10));
		POLYGON_T.push(new Point(LB_X - 10, 30));
		POLYGON_T.push(new Point(20 + STAT_WIDTH, 30));
		POLYGON_T_WIDTH = LB_X - 29 - STAT_WIDTH;
		POLYGON_M = new Polygon([]);
		POLYGON_M.push(new Point(10, canvas.height - 10 - MINI_MAP_SIZE));
		POLYGON_M.push(new Point(10 + MINI_MAP_SIZE, canvas.height - 10 - MINI_MAP_SIZE));
		POLYGON_M.push(new Point(10 + MINI_MAP_SIZE, canvas.height - 10));
		POLYGON_M.push(new Point(10, canvas.height - 10));
		refreshPolygonT2();
	}

/* Evénements */
	// Redimensionnement de la fenêtre
	addEventListener('resize', function() {
		setCanvas();
	});

	// Déplacement de la souris
	addEventListener('mousemove', function(event) {
		tab_player[ID_PLAYER].interaction.setCursorPosition(event, map.ref);
	});

	// Appuis sur une ou plusieurs touches
	addEventListener('keydown', function(event) {
		tab_player[ID_PLAYER].interaction.refreshKeyState(event.keyCode, true);

		if (event.keyCode == 17) HITBOX = (HITBOX+1)%2;
		if (event.keyCode == 16) tab_player[ID_PLAYER].animation_on = 
			(tab_player[ID_PLAYER].animation_on+1)%2;
		if (event.keyCode == 13) {
			tab_player.push(new Player('Intru n°' + tab_player.length, 
				random(0, 6), new Point(map.center)));
			ID_PLAYER = tab_player.length - 1;
		}
		if (event.keyCode == 107) ID_PLAYER = (ID_PLAYER+1)%tab_player.length;
		if (event.keyCode == 109) ID_PLAYER = (ID_PLAYER+tab_player.length-1)%tab_player.length;
		if (event.keyCode == 27) {
//			var text = tab_player[0].name + '\n';
//			for (var k = 0; k < tab_player[0].ship.hitbox.length; k++) {
//				text += Math.round(tab_player[0].ship.hitbox[k].x) + ' : ' +
//					Math.round(tab_player[0].ship.hitbox[k].y) + '\n';
//			}
//			text += '\n' + tab_player[1].name + '\n'
//			for (var k = 0; k < tab_player[1].ship.hitbox.length; k++) {
//				text += Math.round(tab_player[1].ship.hitbox[k].x) + ' : ' +
//					Math.round(tab_player[1].ship.hitbox[k].y) + '\n';
//			}
//			text += '\nCollision : ' + tab_player[0].shipCollision(tab_player[1].ship);				
//			alert(text);
			alert('pause');
		}
	});
	// Relachement d'une ou plusieurs touches
	addEventListener('keyup', function(event) {
		tab_player[ID_PLAYER].interaction.refreshKeyState(event.keyCode, false);
	});

	// Appuis sur un ou plusieurs boutons de souris
	addEventListener('mousedown', function() {
		tab_player[ID_PLAYER].interaction.setKeyState('shoot', true);
		tab_player[ID_PLAYER].interaction.setClicShoot(true);
		tab_player[ID_PLAYER].interaction.setCursorActivation(true);
	});
	// Relachement d'un ou plusieurs boutons de souris
	addEventListener('mouseup', function() {
		tab_player[ID_PLAYER].interaction.setKeyState('shoot', false);
	});


/* Constantes */
	// Universelles
	const PI2 		= Math.PI*2;
	const PI2_3 	= Math.PI*2/3;
	const PI2_5 	= Math.PI*2/5;
	const PI 		= Math.PI;
	const PI_2 		= Math.PI/2;
	const PI_3 		= Math.PI/3;
	const PI_4 		= Math.PI/4;
	const PI_5 		= Math.PI/5;
	const PI_6 		= Math.PI/6;
	const PI_7_5 	= Math.PI/7.5;
	const PI_10 	= Math.PI/10;
	const PI_16 	= Math.PI/16;
	const PI_36 	= Math.PI/36;
	const PI_128 	= Math.PI/128;

	// Moteur
	const CPS = 30;
	const FPS = 60;
	var engine;
	var min_engine = 1000;
	var max_engine = 0;
	var HITBOX = 0;
	var display;
	var min_display = 1000;
	var max_display = 0;

	// Map
	const NB_ZONE = 8;
	const ZONE_SIZE = 240;
	const MAP_SIZE = NB_ZONE*ZONE_SIZE;
	const STAR_BY_ZONE = 15;
	const DEEP = 10;
	const NB_BLACK_HOLE = 2;
	const BLACK_HOLE_MAX_SIZE = 150;
	const ZERO = new Point();

	// Vaisseau
	const SPEED_MAX = 6;
	const ACCELERATION_COEFF = .12;
	const FRICTION_COEFF = .96;
	const ROTATION_SENSIBIITY = .25;
	const MIN_TIME_SHOOT = 5;	// ou 4
	const MAX_TIME_SHOOT = 7;
	const TIME_BLINK = 1*CPS;

	const BULLET_SPEED = 20;
	
	// Astéroïds
	const FREQ_ASTEROID = 10;
	const ASTEROID_MAX_SIZE = 4;
	const NB_SIDE_MIN = 5;
	const NB_SIDE_MAX = 7;

	// Bonus
	const FREQ_BONUS = 2; //8
	const BONUS_ACCELERATION = 1.2;
	const BONUS_TIME_OUT = 10*CPS;	// secondes
	const BONUS_NB = 6;
	const TAB_BONUS_COEFF= [1, .75, 2.5, 2.5, 1, .75, 3];

	// Tableau des scores
	const STAT_TEXT = 15;
	const STAT_WIDTH = STAT_TEXT*11.9;
	const LB_Y = 10;
	const NB_LB = 10;	
	var POLYGON_LB;
	var POLYGON_T;
	var POLYGON_T_WIDTH;
	var POLYGON_T2;
	var POLYGON_P = new Polygon([]);
	POLYGON_P.push(new Point(10, 10));
	POLYGON_P.push(new Point(10 + STAT_WIDTH, 10));
	POLYGON_P.push(new Point(10 + STAT_WIDTH, 16 + (STAT_TEXT + 6)*NB_LB));
	POLYGON_P.push(new Point(10, 16 + (STAT_TEXT + 6)*NB_LB));
	var POLYGON_M;
	const MINI_MAP_SIZE = 300;
	const SCALE = MAP_SIZE/MINI_MAP_SIZE;

	// Pseudo
	const FONT = 'DejaVu sans Mono';
	const PSEUDO_TEXT = 12;

	// Variable du jeu
	var map = new Map(MAP_SIZE);
	var tab_asteroid = [];
	var tab_player = [];

	var ID_PLAYER = 0;

	tab_player.push(new Player('Kirafi', random(0, 6), new Point(map.center)));
//	tab_player[ID_PLAYER].bonus = new Bonus(random(BONUS_NB, BONUS_NB), tab_player[ID_PLAYER].ship.origin);

	var time = 0;
	const TIME_OVER = 3*60*CPS;
	refreshPolygonT2();

	function refreshRank() {
		current_player = tab_player[ID_PLAYER];
		tab_player.sort(function(player, player_2) {
			return player_2.stat.score - player.stat.score;
		});
		for (var i = 0; i < tab_player.length; i++) {
			tab_player[i].stat.rank = i;
			if (tab_player[i] == current_player) {
				ID_PLAYER = i;
			}
		}
	}

	function getPlayerRank(rank) {
		return tab_player.find(function (player) {
			return player.stat.rank == rank;
		});
	}

	function refreshPolygonT2() {
		POLYGON_T2 = new Polygon([]);
		POLYGON_T2.push(new Point(21 + STAT_WIDTH, 11));
		POLYGON_T2.push(new Point(19 + STAT_WIDTH + POLYGON_T_WIDTH*time/TIME_OVER, 11));
		POLYGON_T2.push(new Point(19 + STAT_WIDTH + POLYGON_T_WIDTH*time/TIME_OVER, 29));
		POLYGON_T2.push(new Point(21 + STAT_WIDTH, 29));
	}

/* Moteur de jeu */
	function engine() {
		engineStart = new Date().getTime();
	// Partie
		if (time < TIME_OVER) {
			time++;
			refreshPolygonT2();
		}

	// Joueur
		for (var player of tab_player) {
		// Déplacement
			// Position et orientation du vaisseau
			player.refreshShip();
//			for (var j = 0; j < 2 && player.ship.vector.coeff > 1; j++) {
//				var point = new Point(player.ship.origin);
//				point.translate(new Vector(18, player.ship.orientation + PI));
//				point.translate(random(-2, 2)*2, random(-2, 2)*2);
//				player.tab_animation.push(new Animation('particle',
//					(j+2)*4, point, player.color, 
//					player.ship.origin.angle(player.ship.vector.direction)));
//			}
		}

		// nouveau référentiel pour la map
		var old_ref = new Point(map.ref);
		map.setReferentiel(tab_player[ID_PLAYER].ship.origin);
		// Actualisation de la position du curseur
		tab_player[ID_PLAYER].refreshCursor(map.ref, old_ref);

		for (var player of tab_player) {
		// Missiles
			// Génération
			player.generateShoot();
			// Missiles à tête chercheuse
			for (var bullet of player.tab_bullet) {
				if (bullet.id == 5 && bullet.origin.range(player.ship.origin) > 50) {
					// Cherche l'astéroïde le plus proche dans un rayon de quart de cercle
					var range = map.size;
					var asteroid_near;
					for (var asteroid of tab_asteroid) {
						if (bullet.origin.range(asteroid.origin) < range &&
							bullet.origin.angle(asteroid.origin) > bullet.orientation - PI_4 &&
							bullet.origin.angle(asteroid.origin) < bullet.orientation + PI_4) {
							range = bullet.origin.range(asteroid.origin);
							asteroid_near = asteroid;
						}
					}
					// Actualisation de la direction vers l'astéroïde trouvé
					if (asteroid_near) {
						bullet.vector.setDirection(bullet.origin.angle(asteroid_near.origin));
						bullet.vector.direction += random(-3, 3)*PI_16;
					}
					// Annimations
					for (var i = 0; i < 3; i++) {
						var point = new Point(bullet.origin);
						point.translate(random(-2, 2)*3, random(-2, 2)*3);
						player.tab_animation.push(new Animation('particle',
							(i+1)*4, point, player.color, bullet.orientation));
					}
					bullet.orientation = bullet.vector.direction;
				}
			}
			// Déplacement
			player.refreshBullet(map.hitbox);
			// Collision avec astéroide
			for (var i = 0; i < player.tab_bullet.length; i++) {
				for (var j = 0; j < tab_asteroid.length; j++) {
					// Si un missile touche un astéroïde
					if (player.tab_bullet[i].origin.content(tab_asteroid[j].hitbox)) {
						// Coupage de l'astéroïde en 2 si c'est pas un petit
						if (tab_asteroid[j].size > 1) {
							var tab_splited = tab_asteroid[j].getTabSplit(
								player.tab_bullet[i].orientation);
							tab_asteroid.push(tab_splited[0]);
							tab_asteroid.push(tab_splited[1]);
						}
						// Sinon si c'est une astéroïde bonus
						else if (tab_asteroid[j].isBonus() && !player.bonus.isDefined())
						{
							player.setBonus(tab_asteroid[j]);
						}
						// Animations
						player.animSplit(player.tab_bullet[i], tab_asteroid[j]);
						// Statistiques
						player.incrementShootStat(player.tab_bullet[i].id, tab_asteroid[j].size);
						player.refreshScore(tab_asteroid[j].getScore());
						// Si c'était le missile à fragmentation
						if (player.tab_bullet[i].id == 6) {
							var vector = player.tab_bullet[i].vector;
							vector.rotate(PI_128*random(0, 20));
							for (var k = 0; k < 6; k++) {
								player.tab_bullet.push(new Bullet(0, player.tab_bullet[i].origin, vector));
								vector.rotate(PI2_5);
							}
						}
						// Mise à jour des tableaux d'astéroïdes et missiles
						tab_asteroid.splice(j, 1);
						player.tab_bullet.splice(i, 1);
						i--;
						break;
					}
				}
			}
			// Collision avec vaisseau
			for (var i = 0; i < player.tab_bullet.length; i++) {
				for (var player_2 of tab_player) {
					//if (player.tab_bullet[i].origin.content(player_2.ship.hitbox)) {
					if (player.tab_bullet[i].origin.range(player_2.ship.origin) < 23 && 
							player != player_2) {
						player.tab_animation.push(new Animation('blast', random(3, 4),  
							player.tab_bullet[i].origin, player.color));

						var point = new Point(player_2.ship.origin);
						point.translate(5*random(-3, 3), 5*random(-3, 3));
						player_2.ship.tab_animation.push(new Animation('blast', random(3, 4) + 1, 
							point, player_2.color));

						player.incrementShootStat(player.tab_bullet[i].id, 0);
						player.refreshScore(5);
						player_2.refreshScore(-5);

						player.tab_bullet.splice(i, 1);
						i--;
						break;
					}
				}
			}
		// Bonus
			player.refreshBonus();
		// Intéractions
			player.interaction.refreshKeyTime();
			player.interaction.refreshCursorActivation();
			player.interaction.refreshAutoShoot();
			player.interaction.refreshShoot();
		}
	// Astéroïdes
		// Génération
		if (time > 0 && time < TIME_OVER) {
			// Depuis les bords de map
			if (!random(0, tab_asteroid.length/FREQ_ASTEROID)) {
				var size = random(1, ASTEROID_MAX_SIZE);
				var side = random(0, 3);
				if (side%2) {
					var origin = new Point(random(0, map.size),
						map.center.x + (side - 2)*(map.center.x));
				}
				else {
					var origin = new Point(map.center.y +
						(side - 1)*(map.center.y),
						random(0, map.size));
				}
				var target = new Point(random(0, map.size), random(0, map.size));
				var vector = new Vector(
					2*(ASTEROID_MAX_SIZE + 1 - size), 
					origin.angle(target));
				var rotation = .0004*random(15, 30)*(random(0,1)*2 - 1);
				var bonus_id = (size == 1 && !random(0, FREQ_BONUS)) ? random(1, BONUS_NB) : 0;
				tab_asteroid.push(new Asteroid(size, origin, vector, rotation, bonus_id));
			}
			// Depuis les trous noirs
			for (var black_hole of map.tab_black_hole) {
				if (black_hole.size > 40 && !random(0, tab_asteroid.length/FREQ_ASTEROID)) {
					var size = random(1, ASTEROID_MAX_SIZE);
					var side = random(0, 3);
					var vector = new Vector(2*(ASTEROID_MAX_SIZE + 1 - size), toRad(random(0, 360)));
					var origin = new Point(black_hole.origin);
					var rotation = .0004*random(15, 30)*(random(0,1)*2 - 1);
					var bonus_id = (size == 1 && !random(0, FREQ_BONUS)) ? random(1, BONUS_NB) : 0;
					tab_asteroid.push(new Asteroid(size, origin, vector, rotation, bonus_id));
				}
			}
		}
			
		// Déplacement
		for (var i = 0; i < tab_asteroid.length; i++) {
			tab_asteroid[i].refreshMove();
			// Destruction si sort de la map ou si il y a eu collision avec le vaisseau
			if (!tab_asteroid[i].origin.content(map.hitbox)) {
				tab_asteroid.splice(i, 1);
				i--;
			}
			else {
				for (var player of tab_player) {
					if (player.asteroidCollision(tab_asteroid[i])) {
						tab_asteroid.splice(i, 1);
						i--;
						break;
					}
				}
			}
		}

	// Trous Noirs
		// Déplacement
		for (var i = 0; i < map.tab_black_hole.length; i++) {
			map.tab_black_hole[i].refresh();
			for (var j = 0; j < map.tab_black_hole.length; j++) {
				if (map.tab_black_hole[i] != map.tab_black_hole[j] &&
					!map.tab_black_hole[j].reachTarget &&
					map.tab_black_hole[i].origin.range(map.tab_black_hole[j].origin) < 
					(map.tab_black_hole[i].size + map.tab_black_hole[j].size)*2) {
					map.tab_black_hole[i].clear();
				}
			}
			if (map.tab_black_hole[i].isOver()) {
				map.tab_black_hole.splice(i, 1);
				i--;
				map.generateBlackHole();
			}
		}
		// Action de gravité sur les entités
		for (var black_hole of map.tab_black_hole) {
			for (var asteroid of tab_asteroid) {
				if (black_hole.origin.range(asteroid.origin) < black_hole.size*1.5) {
					var vector = new Vector(
						asteroid.vector.coeff/7.5,
						black_hole.origin.angle(asteroid.origin));
					asteroid.vector.sum(vector);
				}
			}
			for (var player of tab_player) {
				if (black_hole.origin.range(player.ship.origin) < black_hole.size*2) {
					var vector = 
						new Vector((player.ship.vector.coeff || player.ship.speed_max)/
							2*(BLACK_HOLE_MAX_SIZE/black_hole.origin.range(player.ship.origin)),
						black_hole.origin.angle(player.ship.origin));
					player.ship.vector.sum(vector);
				}
				for (var i = 0; i < player.tab_bullet.length; i++) {
					if (black_hole.origin.range(player.tab_bullet[i].origin) < black_hole.size) {
						player.tab_animation.push(new Animation('blast', random(2, 3),  
							player.tab_bullet[i].origin, player.color));
						for (var j = 0; j < random(5, 8); j++) {
							var point = new Point(player.tab_bullet[i].origin);
							point.translate(random(-4, 4)*5, random(-4, 4)*5);
							black_hole.tab_animation.push(new Animation('particle',
								(j+1)*4, point, player.color, 
								black_hole.origin.angle(player.tab_bullet[i].origin)));
						}
						player.tab_bullet.splice(i, 1);
						i--;
					}
				}
			}
		}
	// Classement
		refreshRank();
	// Animation
		for (var player of tab_player) {
			player.refreshAnimation()
			if (player.isBlinking()) {
				player.timeFlows();
			}
		}
		for (var black_hole of map.tab_black_hole) {
			black_hole.tab_animation.refresh();
		}

		engine = new Date().getTime() - engineStart;
		if (max_engine < engine) max_engine = engine;
		if (min_engine > engine) min_engine = engine;
	}

/* Affichage */
	function drawStat(text, text2, type, x, x2, y) {
		ctx.fillText(text, x, y);
		if (typeof text2 == 'number') {
			var text_number = new Intl.NumberFormat().format(text2);
		}
		else {
			var text_number = text2;
		}
		text_number += (type) ? ' ' + type : '';
		ctx.fillText(text_number, x2 - ctx.measureText(text_number).width, y);
	}

	function drawMini(point, size, color, border) {
		ctx.fillStyle = color;
		ctx.beginPath();
		ctx.arc(POLYGON_M[0].x + MINI_MAP_SIZE*point.x/map.size, 
			POLYGON_M[0].y + MINI_MAP_SIZE*point.y/map.size, size, 0, PI2);
		ctx.fill();
		if (border) {
			ctx.strokeStyle = 'white';
			ctx.lineWidth = .25;
			ctx.stroke();
		}
	}

	function display() {
		var displayStart = new Date().getTime();

		ctx.clearRect(0, 0, canvas.width, canvas.height);

		// Map
		map.draw(map.ref, tab_player[ID_PLAYER].animation_on);

		if (!HITBOX) {
			// Astéroïdes
			for (var i = tab_asteroid.length - 1; i >= 0; i--) {
				var opacity = 1;
				for (var j = 0; j < map.tab_black_hole.length; j++) {
					if (map.tab_black_hole[j].origin.range(tab_asteroid[i].origin) < 
							map.tab_black_hole[j].size/2) {
						opacity = (map.tab_black_hole[j].origin.range(tab_asteroid[i].origin))/
							map.tab_black_hole[j].size*2;
					}
				}
				tab_asteroid[i].draw(map.ref, opacity);
			}
			// Joueur
			for (var i = 0; i < tab_player.length; i++) {
				// Vaisseau
				if (!tab_player[i].isBlinking() || (tab_player[i].time_blink/2)%3 == 0) {
					tab_player[i].ship.draw(map.ref, tab_player[i].color, tab_player[i].bonus);
				}
				// Missiles
				for (var j = 0; j < tab_player[i].tab_bullet.length; j++) {
					tab_player[i].tab_bullet[j].draw(map.ref, tab_player[i].color);
				}
				// Bonus
				if (tab_player[i].bonus.isDefined() && !tab_player[i].bonus.isActivated()) {
					tab_player[i].bonus.draw(map.ref);
				}
			}
		}
		else {
			// Astéroïdes
			for (var i = 0; i < tab_asteroid.length; i++) {
				if (tab_asteroid[i].isBonus()) {
					tab_asteroid[i].hitbox.draw(
						map.ref, getFillColor(tab_asteroid[i].bonus_id, 200), 'fill');
				}
				else {
					tab_asteroid[i].hitbox.draw(map.ref, 'dimgray', 'fill');
				}
			}
			for (var i = 0; i < tab_player.length; i++) {
				// Missiles
				for (var bullet of tab_player[i].tab_bullet) {
					bullet.drawHitbox(map.ref, getFillColor(tab_player[i].color, 175), 'fill');
				}
				// Vaisseau
				tab_player[i].ship.hitbox.draw(map.ref, getFillColor(tab_player[i].color, 175), 'fill');
				// Bonus
				tab_player[i].bonus.drawHitbox(
					map.ref, getFillColor(tab_player[i].bonus.id, 100), 'fill');
				// Collision visible
				for (var j = 0; j < tab_asteroid.length; j++) {
					if (tab_player[i].ship.hitbox.collision(tab_asteroid[j].hitbox)) {
						tab_asteroid[j].hitbox.draw(map.ref, 'cyan', 'stroke', 3);
						tab_player[i].ship.hitbox.draw(map.ref, 'lime', 'stroke', 3);
					}
					for (var k = j+1; k < tab_asteroid.length; k++) {
						if (tab_asteroid[k].hitbox.collision(tab_asteroid[j].hitbox)) {
							tab_asteroid[k].hitbox.draw(map.ref, 'yellow', 'stroke', 1);
							tab_asteroid[j].hitbox.draw(map.ref, 'yellow', 'stroke', 1);
						}
					}
				}
				for (var j = 0; j < tab_player.length; j++) {
					//if (player.ship.hitbox.collision(player_2.ship.hitbox)) {
					if (tab_player[i].ship.origin.range(tab_player[j].ship.origin) < 23 && 
							tab_player[i] != tab_player[j]) {
						tab_player[i].ship.hitbox.draw(map.ref, 'red', 'stroke', 3);
						tab_player[j].ship.hitbox.draw(map.ref, 'red', 'stroke', 3);
					}
				}
			}
		}

		// Animations
		if (tab_player[ID_PLAYER].animation_on) {
			for (var i = 0; i < tab_player.length; i++) {
				tab_player[i].tab_animation.draw(map.ref);
				tab_player[i].ship.tab_animation.draw(map.ref);
			}
			for (var i = 0; i < map.tab_black_hole.length; i++) {
				map.tab_black_hole[i].tab_animation.draw(map.ref);
			}
		}

		// Pseudo et Score du joueur sous le vaisseau
		for (var i = 0; i < tab_player.length; i++) {
			tab_player[i].draw(map.ref);
		}
		// Interface
		// Joueur
		POLYGON_P.draw(ZERO, 'rgba(0, 0, 0, .6)', 'fill');
		POLYGON_P.draw(ZERO, 'rgba(255, 255, 255, .6)', 'stroke', 1);
		ctx.fillStyle = getFillColor(tab_player[ID_PLAYER].color, 175);
		var text_auto_shoot = (tab_player[ID_PLAYER].interaction.auto_shoot) ? 'ON' : 'OFF';
		const tab_stat = [
			[tab_player[ID_PLAYER].name		, tab_player[ID_PLAYER].stat.rank+1	],
			['Score'			, Math.floor(tab_player[ID_PLAYER].stat.score)	],
			['Score Max'		, tab_player[ID_PLAYER].stat.score_max			],
			['Score Total'		, tab_player[ID_PLAYER].stat.score_total		],
			[''					, ''											],
			['Astéroïdes'		, tab_player[ID_PLAYER].stat.nb_asteroid		],
			['Précison'			, tab_player[ID_PLAYER].stat.precision			],
			['Collisions'		, tab_player[ID_PLAYER].stat.collision			],
			[''					, ''											],
			['Tir Automatique'	, text_auto_shoot								]];
		var x2 = 2 + STAT_WIDTH;
		var y = 28;
		const gap = STAT_TEXT + 6;
		for (var stat of tab_stat) {
			var type = '';
			if (stat[0] == tab_player[ID_PLAYER].name) {
				type = '/ ' + tab_player.length;
			}
			else if (stat[0] == 'Précison') {
				type = '%';
			}
			drawStat(stat[0], stat[1], type, 18, x2, y);
			y += gap;
		}
		// Classement
		POLYGON_LB.draw(ZERO, 'rgba(0, 0, 0, .6)', 'fill');
		POLYGON_LB.draw(ZERO, 'rgba(255, 255, 255, .6)', 'stroke', 1);
		ctx.font = STAT_TEXT + 'px ' + FONT;
		var x = LB_X + 8;
		var y = 28;
		var x2 = LB_X + STAT_WIDTH - 8;
		for (i = 0; i < NB_LB; i++) {
			var player = getPlayerRank(i);
			if (typeof player != 'undefined') {
				ctx.fillStyle = getFillColor(player.color, 175);
				if (player == tab_player[ID_PLAYER]) {
					var rect = new Polygon(
						[LB_X			, LB_X+STAT_WIDTH	,  LB_X+STAT_WIDTH	,  LB_X],
						[y-STAT_TEXT	, y-STAT_TEXT	, y+5	, y+5]);
					if (player.stat.rank == 0) {
						rect[0].y -= 3;
						rect[1].y -= 3;
					}
					else if (player.stat.rank == 9) {
 						rect[2].y += 3;
						rect[3].y += 3;
					}
					rect.draw(ZERO, getFillColor(player.color, 175, .6), 'fill');
					rect.draw(ZERO, 'rgba(255, 255, 255, .6)', 'stroke', 1);
					ctx.fillStyle = 'black';
					drawStat(player.name, Math.floor(player.stat.score), '', x-1, x2+1, y);
				}				
				drawStat(player.name, Math.floor(player.stat.score), '', x, x2, y);
				y += gap;
			}
		}
		ctx.font = PSEUDO_TEXT + 'px ' + FONT;
		// Temps
		POLYGON_T.draw(ZERO, 'rgba(0, 0, 0, .6)', 'fill');
		POLYGON_T.draw(ZERO, 'rgba(255, 255, 255, .6)', 'stroke', 1);
		POLYGON_T2.draw(ZERO, getFillColor(tab_player[ID_PLAYER].color, 100, .4), 'fill');
		POLYGON_T2.draw(ZERO, 'rgba(255, 255, 255, .6)', 'stroke', 1);
		// Mini-Map
		POLYGON_M.draw(ZERO, 'black', 'fill');
		POLYGON_M.draw(ZERO, 'white', 'stroke', 1);
		for (var i = 0; i < map.tab_black_hole.length; i++) {
			drawMini(map.tab_black_hole[i].origin, map.tab_black_hole[i].size/SCALE, 'black', 1);
		}
		for (var i = 0; i < tab_asteroid.length; i++) {
			drawMini(tab_asteroid[i].origin, tab_asteroid[i].size*.75, 'rgb(75, 75, 75)');
		}
		for (var i =0; i < tab_player.length; i++) {
			drawMini(tab_player[i].ship.origin, 3, getFillColor(tab_player[i].color, 100), 
				i == ID_PLAYER);
			for (var j = 0; j < tab_player[i].tab_bullet.length; j++) {
				drawMini(tab_player[i].tab_bullet[j].origin, .5, getFillColor(tab_player[i].color, 200));
			}
		}

		display = new Date().getTime() - displayStart;
		if (max_display < display) max_display = display;
		if (min_display > display) min_display = display;
		// Informations diverses*/
		//devWindow();
	}

/* Fenêtre d'informations */
	function devWindow() {

		var n = 0;
		for (var i = 0; i < tab_asteroid.length; i++) {
			if (tab_asteroid[i].isBonus()) n++;
		}

		dev_window = open('', 'Developper_Window', 'width=400, height=800, left=0');
		dev_window.document.body.innerHTML 
		= '<style>'
			+ 'p {'
				+ 'font-family : ' + 'DejaVu sans Mono' + ';'
				+ 'margin: 2px 10px;'
				+ 'font-weight: bold;'
			+ '}'
		+ '</style>'
		+ '<body">'
			+ '<p>cursor   : ' + tab_player[ID_PLAYER].interaction.cursor_activate		+ '</p>'
			+ '<p>up       : ' + tab_player[ID_PLAYER].interaction.keyTime.up			+ '</p>'
			+ '<p>down     : ' + tab_player[ID_PLAYER].interaction.keyTime.down			+ '</p>'
			+ '<p>left     : ' + tab_player[ID_PLAYER].interaction.keyTime.left			+ '</p>'
			+ '<p>right    : ' + tab_player[ID_PLAYER].interaction.keyTime.right		+ '</p>'
			+ '<p>r_left   : ' + tab_player[ID_PLAYER].interaction.keyTime.rotate_left	+ '</p>'
			+ '<p>r_right  : ' + tab_player[ID_PLAYER].interaction.keyTime.rotate_right	+ '</p>'
			+ '<p>t_blink  : ' + tab_player[ID_PLAYER].time_blink						+ '</p>'
			+ '<p>----------------</p>'
			+ '<p>ship_speed : ' + tab_player[ID_PLAYER].ship.vector.coeff	+ '</p>'
			+ '<p>----------------</p>'
			+ '<p>clic_shoot  : ' + tab_player[ID_PLAYER].interaction.clic_shoot				+ '</p>'
			+ '<p>clic_next   : ' + tab_player[ID_PLAYER].interaction.clic_next					+ '</p>'
			+ '<p>shoot       : ' + tab_player[ID_PLAYER].interaction.keyTime.shoot				+ '</p>'
			+ '<p>time_shoot  : ' + tab_player[ID_PLAYER].interaction.time_shoot				+ '</p>'
			+ '<p>min_t_shoot : ' + tab_player[ID_PLAYER].interaction.min_time_shoot			+ '</p>'
			+ '<p>max_t_shoot : ' + tab_player[ID_PLAYER].interaction.max_time_shoot			+ '</p>'
			+ '<p>switch_auto : ' + tab_player[ID_PLAYER].interaction.keyTime.switch_auto_shoot	+ '</p>'
			+ '<p>auto_shoot  : ' + tab_player[ID_PLAYER].interaction.auto_shoot				+ '</p>'
			+ '<p>------------------</p>'
			+ '<p>bonus_id         : ' + tab_player[ID_PLAYER].bonus.id						+ '</p>'
			+ '<p>bonus_activate   : ' + tab_player[ID_PLAYER].bonus.isActivated()			+ '</p>'
			+ '<p>bonus_time (sec) : ' + Math.floor(tab_player[ID_PLAYER].bonus.time/30)	+ '</p>'
			+ '<p>------------------</p>'
			+ '<p>engine      : ' + engine + '</p>'
			+ '<p>min_engine  : ' + min_engine + '</p>'
			+ '<p>max_engine  : ' + max_engine + '</p>'
			+ '<p>fps         : ' + fps.getFPS() + '</p>'
			+ '<p>display     : ' + display + '</p>'
			+ '<p>min_display : ' + min_display + '</p>'
			+ '<p>max_display : ' + max_display + '</p>'
			+ '<p>-------------</p>'
			+ '<p>nb_asteroid : ' + tab_asteroid.length + '</p>'
			+ '<p>nb_as_bonus : ' + n + '</p>'
			+ '<p>nb_bullet   : ' + tab_player[ID_PLAYER].tab_bullet.length + '</p>'
			+ '<p>nb_anim     : ' + (tab_player[ID_PLAYER].tab_animation.length + 
				tab_player[ID_PLAYER].ship.tab_animation.length) + '</p>'
			+ '<p>-------------</p>'
			+ '<p>time        : ' + time + '</p>'
			+ '<p>time_over   : ' + TIME_OVER + '</p>'
		+ '</body>';
	}


/* Initialisation de la fenêtre au lancement */
	setCanvas();

/* Timers */
	var game = {
		calcul : setInterval(engine, 1000/CPS),
		frame : setInterval(display, 1000/FPS)
	};
}