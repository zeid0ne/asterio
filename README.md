# Asterio

Online multiplayer space shooter game.

## Setup

### Developpement

- Install dependencies `npm install`
- Hot reload compilation `npm test`

### Solo

- Open `public/indexSolo.html` in browser

### Mutli

- Start server `npm start`
- And connect to localhost:4004

## Ideas

### Small Asteroid

When small asteroids break they leave piece of asteroids.

They are pushed by other entities (dont hit ships) and juste obstruct shoot (no point if a player shoot them).

### Ulti

big bullet wave or big wave push other entities with limited radius (like 100 pixel around the ship).

Reload when player touch asteroid with his bullets

## Client side prediction for bullet shoot

All code with `// CSP` (Client Side Prediction) is disabled for now.

Generate BlakHoles & Asteroids, and maybe Bullets with a precise date to syncronise all clients.

Maybe make the same system for destruction.

## Usefull links

- [Time precision on Firefox](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Date/now#Pr%C3%A9cision_temporelle_r%C3%A9duite)
